<?php

/*
|--------------------------------------------------------------------------
| Register The Application Constant Variables
|--------------------------------------------------------------------------
*/

define ('APP_NAME', 'Dealr');
define ('APP_VERSION', '0.1b');

define ('ENTITY_NAMESPACE_BASE', 'Devio\\Entities');
define ('ENTITY_NAMESPACE_PATH', '%s\\%s');