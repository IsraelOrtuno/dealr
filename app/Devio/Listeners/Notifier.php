<?php namespace Devio\Listeners; 

use Devio\Entities\Company\Events\CompanyWasCreated;

class Notifier {

    public function whenCompanyWasCreated(CompanyWasCreated $event)
    {
        dd('Company created:' . $event->company->name);
    }

} 