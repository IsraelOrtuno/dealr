<?php namespace Devio\Entities\Config\Collections\Repositories; 

use Devio\EavModel\Models\EavCollectionItem;
use Devio\Support\Repository\TenantRepository;

class CollectionRepository extends TenantRepository implements CollectionRepositoryInterface {

    protected $entity = 'Devio\EavModel\Models\EavCollection';

    /**
     * @param $id
     */
    public function findItem($id)
    {
        return EavCollectionItem::find($id);
    }
    
    /**
     * @param $collection
     * @param $value
     *
     * @return mixed
     */
    public function addItem($collection, $value)
    {
        $collection = $this->find($collection);

        return $collection->add($value);
    }

    /**
     * @param $id
     * @param $value
     */
    public function updateItem($id, $value)
    {
        $item = EavCollectionItem::find($id);

        $item->value = $value;

        $item->save();
    }

    /**
     * @param $id
     *
     * @return int
     */
    public function deleteItem($id)
    {
        return EavCollectionItem::destroy($id);
    }

} 