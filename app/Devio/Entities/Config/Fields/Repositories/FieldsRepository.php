<?php namespace Devio\Entities\Config\Fields\Repositories;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Devio\Support\Repository\TenantRepository;

class FieldsRepository extends TenantRepository implements FieldsRepositoryInterface {

    protected $entity = 'Devio\EavModel\Models\EavField';

    /**
     * Returns a unique collection merging the custom fields from the database
     * and the base fields set into the application config file.
     *
     * @param $entity
     *
     * @return Collection
     */
    public function getAllFields($entity)
    {
        $baseFields = new Collection(Config::get('entity.fields.'.$entity));
        $customFields = instantiate_entity($entity)->fields;

        return $baseFields->merge($customFields);
    }

    /**
     * @param array $input
     *
     * @return mixed
     */
//    public function create(array $input = array())
//    {
//        $input['account_id'] = 1;
//
//        $field = EntityFieldsMeta::create($input);
//
//        return $field;
//    }

    /**
     * @param       $id
     * @param array $input
     */
//    public function update($id, array $input = array())
//    {
//
//    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function delete($id)
    {
        $field = $this->find($id, 'value');

        if ($field->value)
            $field->value->delete();

        return $field->delete();
    }

} 