<?php namespace Devio\Entities\Company;

use Eloquent;
use Devio\EavModel\EavModelingTrait;
use Laracasts\Presenter\PresentableTrait;
use Devio\Support\Eloquent\Traits\EntityTrait;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Devio\Support\Eloquent\Traits\ActivityRelationshipTrait;

class Company extends Eloquent {

    use EntityTrait;
    use EavModelingTrait;
    use PresentableTrait;
    use SoftDeletingTrait;
    use ActivityRelationshipTrait;

    /**
     * Entity Presenter
     *
     * @var string
     */
    protected $presenter = 'Devio\\Presenters\\CompanyPresenter';

    /**
     * List of fillable attributes
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description'
    ];

    /**
     * Eager loading the fields relationship on every query. Note this might affect
     * performance, can work without this.
     *
     * @var array
     */
    protected $with = ['fields', 'values'];

    /**
     * Morph Class Name
     *
     * @var string
     */
    protected $morphClass = 'Company';

    /**
     * Relationship with contacts. A company may have many
     * contacts.
     *
     * @return mixed
     */
    public function contacts()
    {
        return $this->hasMany(enp('Contact'));
    }

    /**
     * Relationship with deals. A company may have many deals
     * related.
     *
     * @return mixed
     */
    public function deals()
    {
        return $this->hasMany(enp('Deal'));
    }

}