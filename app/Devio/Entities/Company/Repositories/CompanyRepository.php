<?php namespace Devio\Entities\Company\Repositories;

use Devio\Support\Repository\BaseRepository;
use Laracasts\Commander\Events\EventGenerator;
use Devio\Entities\Company\Events\CompanyWasCreated;
use Devio\Entities\Company\Events\CompanyWasUpdated;

class CompanyRepository extends BaseRepository implements CompanyRepositoryInterface {

    use EventGenerator;

    protected $entity = 'Devio\Entities\Company\Company';

    protected $listeners = [
        'store'  => ['Devio\Listeners\Notifier'],
        'update' => ['Devio\Listeners\Notifier']
    ];


    /**
     * @param array $data
     *
     * @return mixed|void
     */
    public function store(Array $data)
    {
        $company = parent::store($data);

        // A new company was created, act as supposed
        $this->raise(new CompanyWasCreated($company));

        return $company;
    }

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed|void
     */
    public function update($id, Array $data)
    {
        $company = parent::update($id, $data);

        // A company was updated, act as supposed
        $this->raise(new CompanyWasUpdated($company));

        return $company;
    }
}