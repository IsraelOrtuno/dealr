<?php namespace Devio\Entities\Company\Commands; 

class CreateCompanyCommand {

    public $name;

    function __construct($name)
    {
        $this->name = $name;
    }
}