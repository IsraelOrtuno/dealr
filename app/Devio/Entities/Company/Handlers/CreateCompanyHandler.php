<?php namespace Devio\Entities\Company\Handlers;

use Devio\CommandBus\Support\CommandHandler;
use Devio\CommandBus\Support\EventDispatcher;
use Devio\Entities\Company\Repositories\CompanyRepositoryInterface;

class CreateCompanyHandler extends CommandHandler {

    /**
     * @var \Devio\Entities\Company\Repositories\CompanyRepositoryInterface
     */
    private $company;

    /**
     * @var EventDispatcher
     */
    protected $dispatcher;

    function __construct(CompanyRepositoryInterface $company, EventDispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
        $this->company = $company;
    }

    /**
     * Handle the command
     *
     * @param $command
     *
     * @return mixed
     */
    public function handle($command)
    {
        $company = $this->company->store(
            ['name' => $command->name]
        );

        $this->dispatcher->dispatch($this->company->releaseEvents());
    }

}