<?php  namespace Devio\Entities\Company\Events;

use Devio\Entities\Company\Company;

class CompanyWasUpdated {

    /**
     * @var \Devio\Entities\Company\Company
     */
    public $company;

    function __construct(Company $company)
    {
        $this->company = $company;
    }


}