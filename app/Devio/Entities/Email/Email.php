<?php namespace Devio\Entities\Email;

use Laracasts\Presenter\PresentableTrait;

class Email extends \Eloquent {

    use PresentableTrait;

    /**
     * Entity Presenter
     *
     * @var string
     */
    protected $presenter = 'Devio\\Entities\\Email\\EmailPresenter';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'email'
    ];

    /**
     * Morph Class Name
     *
     * @var string
     */
    protected $morphClass = 'Email';

    /**
     * Polymorphic Relationship
     *
     * @return
     */
    public function mailable()
    {
        return $this->morphTo();
    }

} 