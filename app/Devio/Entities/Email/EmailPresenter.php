<?php namespace Devio\Entities\Email;

use Laracasts\Presenter\Presenter;

class EmailPresenter extends Presenter {

    /**
     * @return string
     */
    public function emailLink()
    {
        return '<a href="mailto:' . $this->email . '">' . $this->email . '</a>';
    }

}