<?php namespace Devio\Entities\User;

use Cartalyst\Sentinel\Users\EloquentUser;
use Laracasts\Presenter\PresentableTrait;

class User extends EloquentUser {

    use PresentableTrait;

    /**
     * Entity Presenter
     *
     * @var string
     */
    protected $presenter = 'Devio\\Entities\\User\\UserPresenter';

}
