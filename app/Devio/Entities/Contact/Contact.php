<?php namespace Devio\Entities\Contact;

use Eloquent;
use Devio\EavModel\EavModelingTrait;
use Laracasts\Presenter\PresentableTrait;
use Devio\Support\Eloquent\Traits\EntityTrait;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Devio\Support\Eloquent\Traits\ActivityRelationshipTrait;

class Contact extends Eloquent {

    use EntityTrait;
    use EavModelingTrait;
    use PresentableTrait;
    use SoftDeletingTrait;
    use ActivityRelationshipTrait;

    /**
     * Entity presenter
     *
     * @var string
     */
    protected $presenter = 'Devio\\Presenters\\ContactPresenter';

    /**
     * List of fillable files
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Morph Class Name
     *
     * @var string
     */
    protected $morphClass = 'Contact';

    /**
     * Relation to the company the contact might belong to.
     *
     * @return mixed
     */
    public function company()
    {
        return $this->belongsTo(enp('Company'), 'company_id');
    }

}