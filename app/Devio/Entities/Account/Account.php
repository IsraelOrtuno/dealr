<?php namespace Devio\Entities\Account; 

class Account extends \Eloquent {

    /**
     * List of fillable attributes
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Relation between the account and all the users that belong
     * to it.
     *
     * @return mixed
     */
    public function users()
    {
        return $this->hasMany(enp('User'));
    }

    /**
     * Relation of all companies that belong to the account.
     *
     * @return mixed
     */
    public function companies()
    {
        return $this->hasMany(enp('Company'));
    }

    /**
     * Relation of all contacts that belong to the account.
     *
     * @return mixed
     */
    public function contacts()
    {
        return $this->hasMany(enp('Contact'));
    }

    /**
     * Relation of all deals that belong to the account.
     *
     * @return mixed
     */
    public function deals()
    {
        return $this->hasMany(enp('Deal'));
    }

} 