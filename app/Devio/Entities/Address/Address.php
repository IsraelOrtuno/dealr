<?php namespace Devio\Entities\Address; 

use Laracasts\Presenter\PresentableTrait;

class Address extends \Eloquent {

    use PresentableTrait;

    /**
     * Entity Presenter
     *
     * @var string
     */
    protected $presenter = 'Devio\\Entities\\Address\\AddressPresenter';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'address',
        'post_code',
        'city',
        'state'
    ];

    /**
     * Morph Class Name
     *
     * @var string
     */
    protected $morphClass = 'Address';

    /**
     * Polymorphic Relationship
     *
     * @return
     */
    public function addressable()
    {
        return $this->morphTo();
    }

} 