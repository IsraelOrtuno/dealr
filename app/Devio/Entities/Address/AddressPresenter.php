<?php namespace Devio\Entities\Address;

use Laracasts\Presenter\Presenter;

class AddressPresenter extends Presenter {

    /**
     * @return string
     */
    public function cityAndState()
    {
        return "{$this->city}, {$this->state}";
    }

    /**
     * @return string
     */
    public function fullAddress()
    {
        return $this->address . '<br/>' . $this->post_code . ' - ' . $this->city . ' - ' . $this->state;
    }

}