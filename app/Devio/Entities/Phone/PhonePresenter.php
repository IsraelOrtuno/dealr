<?php namespace Devio\Entities\Phone;

use Laracasts\Presenter\Presenter;

class PhonePresenter extends Presenter {

    /**
     * @return string
     */
    public function phoneLink()
    {
        return '<a href="tel:' . $this->phone . '">' . $this->phone . '</a>';
    }

}