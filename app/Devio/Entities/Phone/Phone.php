<?php namespace Devio\Entities\Phone;

use Laracasts\Presenter\PresentableTrait;

class Phone extends \Eloquent {

    use PresentableTrait;

    /**
     * Entity Presenter
     *
     * @var string
     */
    protected $presenter = 'Devio\\Entities\\Phone\\PhonePresenter';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'phone'
    ];

    /**
     * Morph Class Name
     *
     * @var string
     */
    protected $morphClass = 'Phone';

    /**
     * Polymorphic Relationship
     *
     * @return
     */
    public function phoneable()
    {
        return $this->morphTo();
    }

} 