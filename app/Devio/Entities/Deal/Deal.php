<?php namespace Devio\Entities\Deal; 

use Eloquent;
use Devio\EavModel\EavModelingTrait;
use Laracasts\Presenter\PresentableTrait;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Devio\Support\Eloquent\Traits\ActivityRelationshipTrait;

class Deal extends Eloquent {

    use PresentableTrait;
    use EavModelingTrait;
    use SoftDeletingTrait;
    use ActivityRelationshipTrait;

    /**
     * Entity presenter
     *
     * @var string
     */
    protected $presenter = 'Devio\\Presenters\\DealPresenter';

    /**
     * List of fillable attributes
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'amount'
    ];

    /**
     * Morph Class Name
     *
     * @var string
     */
    protected $morphClass = 'Deal';

} 