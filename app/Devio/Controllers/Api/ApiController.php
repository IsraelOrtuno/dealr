<?php namespace Devio\Controllers\Api; 

use Controller;

class ApiController extends Controller {

    /**
     * Sets the status code for the current response
     *
     * @var integer
     */
    protected $statusCode = 200;

    /**
     * Returns a basic json response with status code, data and headers
     *
     * @param $data
     * @param array $headers
     * @return Response
     */
    public function respond($data, $headers = array())
    {
        // $data['status_code'] = $this->getStatusCode();

        return \Response::json($data, $this->getStatusCode(), $headers);
    }

    /**
     * The respond has found an error
     *
     * @param $message
     * @return Response
     */
    public function respondWithError($message)
    {
        return $this->respond([
                                  'error' => [
                                      'message' => $message,
                                      'status_code' => $this->getStatusCode()
                                  ]
                              ]);
    }

    /**
     * The server has found an array of errors
     *
     * @param $errors
     * @return Response
     */
    public function respondWithErrors($errors)
    {
        return $this->respond([
                                  'errors'  => $errors,
                                  'status_code' => $this->getStatusCode()
                              ]);
    }

    /**
     * The respond has not been found
     *
     * @param string $message
     * @return Response
     */
    public function respondNotFound($message = 'Not found!')
    {
        return $this->setStatusCode(404)->respondWithError($message);
    }

    /**
     * @param $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

}