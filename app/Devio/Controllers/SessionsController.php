<?php namespace Devio\Controllers;

use View;
use Input;
use Sentinel;
use Redirect;

class SessionsController extends BaseController {

    /**
     * Custom filters that apply to the controller.
     */
    protected function applyFilters()
    {
        $this->beforeFilter('auth', ['only' => 'unauthenticate']);
    }

    /**
     * @return mixed
     */
    public function login()
    {
        if (Sentinel::check())
            return Redirect::home();

        return View::make('Session::login');
    }

    /**
     * Authenticating a user. Custom login action.
     */
    public function authenticate()
    {
        $credentials = [
            'email'    => Input::get('email'),
            'password' => Input::get('password')
        ];

        $remember = Input::get('remember') ?: false;

        if (Sentinel::authenticate($credentials, $remember))
        {
            return Redirect::intended('/');
        }

        return Redirect::back()->withErrors('Nombre de usuario o contraseña erróneos.');
    }

    /**
     * Unauthenticate a user from the application. Logout.
     */
    public function unauthenticate()
    {
        Sentinel::logout(null, true);

        return Redirect::route('login');
    }
} 