<?php namespace Devio\Controllers;

use Illuminate\Support\Facades\View;
use Devio\Entities\Company\Repositories\CompanyRepositoryInterface;

class CompaniesController extends BaseController {

    /**
     * @var CompanyRepositoryInterface
     */
    private $company;

    function __construct(CompanyRepositoryInterface $company)
    {
        $this->company = $company;

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     * GET /company
     *
     * @return Response
     */
    public function index()
    {
        $companies = $this->company->allPaginated(8);

        return View::make('Company::index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     * GET /company/create
     *
     * @return Response
     */
    public function create()
    {
        $input = ['name' => 'De prueba'];

        $command = new CreateCompanyCommand($input['name']);

        $this->commandBus->execute($command);
    }

    /**
     * Store a newly created resource in storage.
     * POST /company
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     * GET /company/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $company = $this->company->find($id);

        return View::make('Company::show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     * GET /company/{id}/edit
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /company/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /company/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}