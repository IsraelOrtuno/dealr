<?php  namespace Devio\Controllers;

use Devio\Events\Jobs\PostJobCommand;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;

class JobsController extends BaseController {

    /**
     * Posting a new job
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::only('title', 'description');

        $command = new PostJobCommand($input['title'], $input['description']);

        $this->commandBus->execute($command);
    }

} 