<?php  namespace Devio\Controllers;

use View;
use Sentinel;
use Illuminate\Routing\Controller;

class BaseController extends Controller {

    /**
     * BaseController constructor
     */
    public function __construct()
    {
        $this->bootController();
    }

    /**
     * Initiates the minimum stuff for the controller to work.
     */
    protected function bootController()
    {
        $this->applyFilters();

        $this->registerSharedVariables();
    }

    /**
     * Apply custom filters. By default only auth. This method might
     * be rewritten at any controller based on its needs.
     */
    protected function applyFilters()
    {
        $this->beforeFilter('auth');
    }

    /**
     * Register some basic variables as shared. This way they're
     * available across any view.
     */
    protected function registerSharedvariables()
    {
        View::share('currentUser', Sentinel::check() ?: null);
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if ( ! is_null($this->layout))
        {
            $this->layout = View::make($this->layout);
        }
    }

} 