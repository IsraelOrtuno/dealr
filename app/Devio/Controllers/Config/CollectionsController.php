<?php namespace Devio\Controllers\Config;

use View;
use Input;
use Redirect;
use Devio\Controllers\BaseController;
use Devio\Entities\Config\Collections\Repositories\CollectionRepositoryInterface;

class CollectionsController extends BaseController {

    /**
     * @var CollectionRepositoryInterface
     */
    private $collections;

    public function __construct(CollectionRepositoryInterface $collections)
    {
        $this->collections = $collections;

        parent::__construct();
    }
    
	/**
	 * Display a listing of the resource.
	 * GET /collections
	 *
	 * @return Response
	 */
	public function index()
	{
        $collections = $this->collections->all();

		return View::make('Config::Collections.index', compact('collections'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /collections/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('Config::Collections.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /collections
	 *
	 * @return Response
	 */
	public function store()
	{
		$this->collections->store(Input::only('name'));

        return Redirect::route('config.collections.index');
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /collections/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $collection = $this->collections->find($id);

		return $this->create()->withCollection($collection);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /collections/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$this->collections->update($id, Input::only('name'));

        return Redirect::route('config.collections.index');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /collections/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}