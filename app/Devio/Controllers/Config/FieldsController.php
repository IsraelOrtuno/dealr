<?php namespace Devio\Controllers\Config;

use View;
use Redirect;
use Devio\Controllers\BaseController;
use Devio\Entities\Config\Fields\Repositories\FieldsRepositoryInterface;

class FieldsController extends BaseController {

    /**
     * @var FieldsRepositoryInterface
     */
    private $repo;

    public function __construct(FieldsRepositoryInterface $repo)
    {
        $this->repo = $repo;

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     * GET /fields
     *
     * @return Response
     */
    public function index($entity = 'company')
    {
        $fields = $this->repo->getAllFields($entity);

        return View::make('Config::Fields.index', compact('fields'));
    }

    /**
     * Show the form for creating a new resource.
     * GET /fields/create
     *
     * @return Response
     */
    public function create()
    {
        $entities = [
            'Company' => ucfirst(trans('main.company')),
            'Contact' => ucfirst(trans('main.contact')),
            'Deal'    => ucfirst(trans('main.deal'))
        ];

//        $field = $this->repo->find($id);

        return View::make('Config::Fields.create', compact('entities'));
    }

    /**
     * Store a newly created resource in storage.
     * POST /fields
     *
     * @return Response
     */
    public function store()
    {
        $field = $this->repo->store(Input::all());

        return Redirect::route('config.fields.index');
    }

    /**
     * Display the specified resource.
     * GET /fields/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     * GET /fields/{id}/edit
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     * PUT /fields/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Before deleting a resource just confirm it.
     * GET /fields/{id}/confirm
     *
     * @param $id
     *
     * @return mixed
     */
    public function confirmDeletion($id)
    {
        $field = $this->repo->find($id);

        return $this->index()->withDeletion($field);
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /fields/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->repo->delete($id);

        return Redirect::route('config.fields.index');
    }

}