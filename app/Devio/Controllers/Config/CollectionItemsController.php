<?php namespace Devio\Controllers\Config; 

use View;
use Input;
use Redirect;
use Devio\Controllers\BaseController;
use Devio\Entities\Config\Collections\Repositories\CollectionRepositoryInterface;

class CollectionItemsController extends BaseController {

    /**
     * @var CollectionRepositoryInterface
     */
    private $collection;

    public function __construct(CollectionRepositoryInterface $collection)
    {
        $this->collection = $collection;

        parent::__construct();
    }

    /**
     * Show the form for creating a new resource.
     * GET /collections/items/create
     *
     * @return Response
     */
    public function create($collectionId)
    {
        $collection = $this->collection->find($collectionId);

        return View::make('Config::Collections.Items.create', compact('collection'));
    }

    /**
     * Store a newly created resource in storage.
     * POST /collections/items
     *
     * @return Response
     */
    public function store($id)
    {
        $this->collection->addItem($id, Input::get('value'));

        return Redirect::route('config.collections.edit', $id);
    }

    /**
     * Show the form for editing the specified resource.
     * GET /collections/items/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $item = $this->collection->findItem($id);
        $collection = $item->collection;

        return View::make('Config::Collections.Items.create', compact('item', 'collection'));
    }

    /**
     * Update the specified resource in storage.
     * PUT /collections/items/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $this->collection->updateItem($id, Input::get('value'));

        return Redirect::route('config.collections.edit', $this->getCollectionId($id));
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /collections/items/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $collectionId = $this->getCollectionId($id);

        $this->collection->deleteItem($id);

        return Redirect::route('config.collections.edit', $collectionId);
    }


    /**
     * @param $itemId
     *
     * @return mixed
     */
    protected function getCollectionId($itemId)
    {
        $item = $this->collection->findItem($itemId);

        return $item->collection->id;
    }
}