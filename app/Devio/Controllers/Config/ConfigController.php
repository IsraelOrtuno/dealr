<?php namespace Devio\Controllers\Config; 

use View;
use Devio\Controllers\BaseController;

class ConfigController extends BaseController {

    /**
     * @return mixed
     */
    public function index()
    {
        return View::make('Config::index');
    }

} 