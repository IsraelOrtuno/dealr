<?php namespace Devio\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->entityBindings();

        $this->configBindings();
    }

    protected function entityBindings()
    {
        // Company repo
        $this->app->bind(
            'Devio\Entities\Company\Repositories\CompanyRepositoryInterface',
            'Devio\Entities\Company\Repositories\CompanyRepository'
        );
    }

    protected function configBindings()
    {
        // Custom Fields Repo
        $this->app->bind(
            'Devio\Entities\Config\Fields\Repositories\FieldsRepositoryInterface',
            'Devio\entities\Config\Fields\Repositories\FieldsRepository'
        );

        // Collections Repo
        $this->app->bind(
            'Devio\Entities\Config\Collections\Repositories\CollectionRepositoryInterface',
            'Devio\Entities\Config\Collections\Repositories\CollectionRepository'
        );
    }

}