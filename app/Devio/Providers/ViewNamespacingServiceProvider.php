<?php namespace Devio\Providers;

use Illuminate\Support\ServiceProvider;

class ViewNamespacingServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Layout
        $this->app['view']->addNamespace('Layout',      devio_path('Views/Layout'));

        // Session
        $this->app['view']->addNamespace('Session',     devio_path('Views/Session'));

        // Company
        $this->app['view']->addNamespace('Company',     devio_path('Views/Company'));

        // Contact
        $this->app['view']->addNamespace('Contact',     devio_path('Views/Contact'));

        // Deal
        $this->app['view']->addNamespace('Deal',        devio_path('Views/Deal'));

        // Support
        $this->app['view']->addNamespace('Support',     devio_path('Views/Support'));

        // Config
        $this->app['view']->addNamespace('Config',      devio_path('Views/Config'));

        // Testing
        $this->app['view']->addNamespace('Testing',     devio_path('Views/Testing'));

    }

}