<?php  namespace Devio\Events\Jobs;

use Devio\Job;
use Devio\CommandBus\Support\EventDispatcher;

class PostJobCommandHandler {

    protected $dispatcher;

    function __construct(EventDispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function handle($command)
    {
        $job = Job::post($command->title, $command->description);

        $this->dispatcher->dispatch($job->releaseEvents());
    }

} 