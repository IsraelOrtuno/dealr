<?php  namespace Devio\Events\Jobs;

class PostJobCommand {

    public $description;

    public $title;

    function __construct($description, $title)
    {
        $this->description = $description;
        $this->title = $title;
    }
}