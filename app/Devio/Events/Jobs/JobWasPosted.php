<?php  namespace Devio\Events\Jobs;

use Devio\Job;

class JobWasPosted {

    /**
     * @var \Devio\Job
     */
    public $job;

    function __construct(Job $job)
    {
        $this->job = $job;
    }


} 