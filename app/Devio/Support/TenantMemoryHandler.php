<?php namespace Devio\Support;

use Devio\Memory\Memory;
use Devio\Memory\MemoryHandler;

class TenantMemoryHandler extends MemoryHandler {

    /**
     * @return mixed
     */
    protected function resolver()
    {
        return Memory::where('account_id', '=', 1);
    }

    /**
     * Saves the item into the database but also sets the account_id for multi-tenancy.
     *
     * @param string $key
     * @param mixed  $value
     * @param bool   $isNew
     *
     * @return bool|void
     */
    protected function save($key, $value, $isNew = false)
    {
        $model = Memory::where('name', '=', $key)->where('account_id', '=', 1)->first();

        if (true === $isNew && is_null($model))
        {
            Memory::create(array(
                               'name'       => $key,
                               'value'      => $value,
                               'account_id' => 1
                           ));
        } else
        {
            $model->value = $value;

            $model->save();
        }
    }

}