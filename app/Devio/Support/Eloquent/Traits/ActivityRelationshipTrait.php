<?php namespace Devio\Support\Eloquent\Traits;

trait ActivityRelationshipTrait {

    /**
     * Polymorphic relationship to activities
     *
     * @return mixed
     */
    public function activities()
    {
        return $this->morphToMany(enp('Activity'), 'activitable');
    }

}