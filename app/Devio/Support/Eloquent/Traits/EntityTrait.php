<?php  namespace Devio\Support\Eloquent\Traits;

trait EntityTrait {

    /**
     * Polymorphic relationship to multiple location addresses
     *
     * @return Relationship
     */
    public function addresses()
    {
        return $this->morphMany(enp('Address'), 'addressable');
    }

    /**
     * Polymorphic relationship to multiple phone numbers
     *
     * @return Relationship
     */
    public function phones()
    {
        return $this->morphMany(enp('Phone'), 'phoneable');
    }

    /**
     * Polymorphic relationship to multiple e-mail addresses
     *
     * @return Relationship
     */
    public function emails()
    {
        return $this->morphMany(enp('Email'), 'emailable');
    }

    /**
     * Attribute that fetches the main address of the element
     *
     * @return string
     */
    public function getMainAddressAttribute()
    {
        if (count($this->addresses))
            return $this->addresses()->first();

        return null;
    }

    /**
     * Attribute that fetches the main phone of the element
     *
     * @return string
     */
    public function getMainPhoneAttribute()
    {
        if (count($this->phones))
            return $this->phones()->first();

        return null;
    }

    /**
     * Attribute that fetches the main e-mail of the element
     *
     * @return string
     */
    public function getMainMailAttribute()
    {
        if (count($this->emails))
            return $this->emails()->first();

        return null;
    }

} 