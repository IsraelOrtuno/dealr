<?php

if ( ! function_exists('devio_path'))
{
    /**
     * Get the path to the Devio folder. If argument is passed, it'll
     * be concatenated tot he base Devio folder.
     *
     * @param string $path
     *
     * @return string
     */
    function devio_path($path = '')
    {
        return app_path('Devio') . ($path ? "/{$path}" : '');
    }
}

if ( ! function_exists('entity_namespaced_path'))
{
    /**
     * Generate the entity path with namespace automatically
     *
     * @param null $entity
     *
     * @return string
     */
    function entity_namespaced_path($entity = null)
    {
        // If no entity specified, returns the entity base namespace
        if (is_null($entity))
            return ENTITY_NAMESPACE_BASE;

        // If specified, concatenates the namespace base to the entity namespace path
        return ENTITY_NAMESPACE_BASE . '\\' . str_replace('%s', studly_case($entity), ENTITY_NAMESPACE_PATH);
    }
}

if ( ! function_exists('enp'))
{
    /**
     * An alias of entity_namespaced_path function in order to make it
     * easier to write and remember.
     *
     * @param null $entity
     *
     * @return string
     */
    function enp($entity = null)
    {
        return entity_namespaced_path($entity);
    }
}

if ( ! function_exists('instantiate_entity'))
{
    /**
     * Returns a new instance of an entity.
     *
     * @param $entity
     *
     * @return mixed
     */
    function instantiate_entity($entity)
    {
        return App::make(enp($entity));
    }
}

if ( ! function_exists('set_active'))
{
    /**
     * Returns the $active parameter if the current route URL is matched.
     * by the $path argument. Is useful for highlighting menu sections.
     *
     * @param        $path
     * @param string $active
     *
     * @return string
     */
    function set_active($path, $active = 'active')
    {
        return Route::is($path . '*') ? $active : '';
//        return Request::is($path) ? $active : '';
    }
}