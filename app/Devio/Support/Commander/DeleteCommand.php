<?php namespace Devio\Support\Commander; 

class DeleteCommand {

    public $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

} 