<?php namespace Devio\Support\Commander; 

class AttributeWithKeyCommand extends AttributeCommand {

    public $id;

    public function __construct($id, $attributes)
    {
        $this->id = $id;

        parent::__construct($attributes);
    }

} 