<?php namespace Devio\Support\Commander;

use Laracasts\Commander\ValidationCommandBus as DefaultValidationCommandBus;

class AttributeValidationCommandBus extends DefaultValidationCommandBus {

    /**
     * If right, validate command data.
     *
     * @param $command
     */
    public function validateCommand($command)
    {
        $validator = $this->commandTranslator->toValidator($command);

        if (class_exists($validator))
        {
            $this->app->make($validator)->validate($command->getAttributes());
        }
    }

} 