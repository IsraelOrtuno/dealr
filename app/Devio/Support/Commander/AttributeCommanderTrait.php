<?php namespace Devio\Support\Commander; 

use ReflectionClass;
use Laracasts\Commander\CommanderTrait;

trait AttributeCommanderTrait {

    use CommanderTrait {
        CommanderTrait::mapInputToCommand as parentMapInputToCommand;
    }

    /**
     * Map an array of input to a command's properties, in this case
     * there is only one, attributes.
     *
     * @param       $command
     * @param array $input
     *
     * @return mixed
     */
    public function mapInputToCommand($command, $input)
    {
        $class = new ReflectionClass($command);

        if ($class->isSubclassOf(AttributeCommand::class))
        {
            $instance = $class->newInstance($input);

            return $instance;
        }

        return $this->parentMapInputToCommand($command, $input);
    }

} 