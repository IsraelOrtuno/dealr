<?php namespace Devio\Support\Commander; 

abstract class AttributeCommand {

    /**
     * @var array
     */
    public $attributes = array();

    public function __construct($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }


} 