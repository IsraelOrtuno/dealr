<?php namespace Devio\Support\Html; 

use Illuminate\Html\HtmlBuilder;

class HtmlBlock extends HtmlBuilder {

    /**
     * @param array $tabs
     *
     * @return string
     */
    public function topTabs($tabs = [])
    {
        $html = '';
        $list = 'ul';

        if (count($tabs) == 0) return $html;

        foreach ($tabs as $index => $tab)
        {
            $attributes = '';

            if ($index == 0)
                $attributes = $this->attributes(['class' => 'active']);

            $html .= "<li{$attributes}>{$tab}</li>";
        }

        $attributes = $this->attributes(['class' => 'nav nav-tabs', 'role' => 'tablist']);

        return "<{$list}{$attributes}>{$html}</{$list}>";
    }

} 