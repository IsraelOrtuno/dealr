<?php  namespace Devio\Support\Contracts;

/**
 * Interface Notifiable
 *
 * Any entity implementing this interface is supposed to be "notifiable" vía email as
 * it's the only method it does include "getEmail".
 *
 * @package Devio\Support\Contracts
 */
interface Notifiable {

    /**
     * Get the email address
     *
     * @return string
     */
    public function getEmail();

} 