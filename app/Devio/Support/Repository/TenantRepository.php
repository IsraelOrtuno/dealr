<?php namespace Devio\Support\Repository;

use Devio\Entities\Account\Account;

abstract class TenantRepository extends BaseRepository {

    /**
     * Account object
     *
     * @var \Devio\Entities\Account\Account
     */
    protected $account;

    public function __construct()
    {
        $this->account = Account::find(1);
    }


    /**
     * Create a new instance of the managed entity related to
     * the current account.
     *
     * @param array $with
     * @return Entity
     */
    public function make($with = array())
    {
        $entity = parent::make($with);

        return $entity->where('account_id', $this->account->id);
    }

    /**
     * Create a new instance for being stored and relates it to
     * the current account.
     *
     * @return Entity
     */
    protected function create() {
        $entity = parent::create();

        $entity->account_id = $this->account->id;

        return $entity;
    }

    /**
     * @return \Devio\Entities\Account\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

}