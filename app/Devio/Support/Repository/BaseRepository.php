<?php namespace Devio\Support\Repository;

use Illuminate\Database\Eloquent\ModelNotFoundException;

abstract class BaseRepository {

    /**
     * Entity path. Here must be set the route to the
     * ORM element that the repository is assigned.
     * For instance: Lion\Tenant.
     *
     * @var string
     */
    protected $entity = null;

    /**
     * Basic dependencies to be included like "with" in the basic
     * queries like find.
     *
     * @var string
     */
    protected $dependences = array();

    /**
     * Event listeners linked
     *
     * @var array
     */
    protected $listeners = array();

    /**
     * Fill created and owned fields
     *
     * @var array
     */
    protected $responsabilityFields = [];

    /**
     * Create a new instance of the managed entity.
     *
     * @param  array  $with
     * @return Entity
     */
    public function make($with = array())
    {
        $entity = new $this->entity;

        return $entity->newQuery()->with($with);
    }

    /**
     * Create a new instance for being stored
     *
     * @return Entity
     */
    protected function create()
    {
        return new $this->entity();
    }

    /**
     * Gets an instance by ID
     *
     * @param  integer|string   $id
     * @param  array            $with
     * @return Entity
     */
    public function find($id, $with = array())
    {
        return $this->make($with)->find($id);
    }

    /**
     * Finds or create an element based. First look for it into the database.
     * If the element is not found, create filling up just the field that
     * has been passed as argument to the method, by default is name.
     *
     * @param $data
     * @param string $field
     * @param array $with
     * @return Entity
     */
    public function findOrCreate($data, $field = 'name', $with = array())
    {
        try {
            // If any item is found just return it or make the app fail
            return $this->make($with)->withTrashed()->findOrFail($data);
        }
        catch (ModelNotFoundException $e)
        {
            // No item was found, create it and return
            return $this->store([$field => $data]);
        }
    }

    /**
     * Gets an instance by ID with all its dependencies
     *
     * @param  integer|string  $id
     * @return Entity
     */
    public function findWithDependences($id)
    {
        return $this->find($id, $this->dependences);
    }

    /**
     * Returns a unique entity with X fields
     *
     * @param  integer  $id
     * @param  array   $fields
     * @return Entity
     */
    public function get($id, $fields = array('*'))
    {
        return $this->make()->where('id', $id)->first($fields);
    }

    /**
     * Get instances by key and value
     *
     * @param  string $key
     * @param  string $value
     * @param  array $fields
     * @param  array  $with
     * @return array
     */
    public function getBy($key, $value, $fields = array('*'), $with = array())
    {
        return $this->make($with)->where($key, $value)->get($fields);
    }

    /**
     * Searches a entity LIKE
     *
     * @param $query
     * @param string $field
     * @param array $fields
     * @return mixed
     */
    public function search($query, $field = 'name', $fields = array('*'))
    {
        return $this->make()->where($field, 'like', "%$query%")->get($fields);
    }

    /**
     * Return all the instances
     *
     * @param  array    $with
     * @return arary
     */
    public function all($with = array())
    {
        return $this->make($with)->get();
    }

    /**
     * Return all the instances paginated
     *
     * @param int $perPage
     * @param array $with
     * @return mixed
     */
    public function allPaginated($perPage = 20, $with = array())
    {
        return $this->make($with)->paginate($perPage);
    }

    /**
     * Lists all the instances based on column and key
     *
     * @param  string $column
     * @param  string $key
     * @return array
     */
    public function lists($column, $key = null)
    {
        return $this->make()->lists($column, $key);
    }

    /**
     * List all the entities ready for a dropdown
     *
     * @param bool $empty
     * @return array
     */
    public function nameList($empty = false)
    {
        $result = $this->lists('name', 'id');

        if ($empty)
            return array('' => '- Seleccione -') + $result;

        return $result;
    }

    /**
     * Return all the instances that have been trashed
     *
     * @param  array    $with
     * @return arary
     */
    public function trashed($with = array())
    {
        return $this->make($with)->onlyTrashed()->get();
    }

    /**
     * Stores a new entity into the database
     *
     * @param  Array  $data
     *
     * @return mixed
     */
    public function store(Array $data)
    {
        // Create new entity
        $entity = $this->create();
        $entity->fill($data);

        foreach ($this->responsabilityFields as $field)
            $entity->$field = null; // \Auth::user()->id;

        $entity->save();

        return $entity;
    }

    /**
     * Updates an entity into the database
     *
     * @param $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, Array $data)
    {
        // Find the entity, update data
        $entity = $this->make()->find($id);
        $entity->fill($data);

        $entity->save();

        return $entity;
    }

    /**
     * Deletes the entity
     *
     * @param  array    $id
     * @return arary
     */
    public function delete($id)
    {
        return $this->find($id)->delete();
    }

    /**
     * Restores the entity
     *
     * @param $id
     * @return mixed
     */
    public function restore($id)
    {
        return $this->find($id)->restore();
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @return string
     */
    public function getDependences()
    {
        return $this->dependences;
    }

}