{{-- Company/index --}}

@extends ('Layout::Main')

@section ('entitybar-title') Listado completo de empresas @stop

@section ('entitybar-content')
<div class="row">
<!--    <div class="col-md-6">-->
<!--        <div class="btn-group mb10">-->
<!--            <a class="btn btn-success" href="#"><i class="fa fa-search fa-fw"></i> Filtro</a>-->
<!--            <a class="btn btn-success" href="#"><i class="fa fa-sort-alpha-asc fa-fw"></i> Orden</a>-->
<!--            <a class="btn btn-success" href="#"><i class="fa fa-expand fa-fw"></i> Exportar</a>-->
<!--        </div>-->
<!--    </div>-->

    <div class="col-md-6">
        <ul class="nav nav-tabs" role="tablist">
            <li><a href="#"><i class="fa fa-plus fa-fw"></i> Crear</a></li>
            <li><a href="#"><i class="fa fa-search fa-fw"></i> Filtro</a></li>
            <li><a href="#"><i class="fa fa-expand fa-fw"></i> Exportar</a></li>
        </ul>
    </div>
</div>

@stop

@section ('content')

<table class="table table-hover table-striped">

    <thead>
        <tr>
            <th>Nombre</th>
        </tr>
    </thead>
    <tbody>

        @foreach ($companies as $company)

        <tr>
            <td>
                {{ link_to_route('companies.show', $company->name, $company->id) }}
            </td>
        </tr>

        @endforeach

    </tbody>

</table>

<div class="pull-right">{{ $companies->links() }}</div>

@stop