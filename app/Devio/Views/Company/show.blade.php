{{-- Company/show --}}

@extends ('Layout::main')

@section ('entitybar-title') {{ $company->name }} @stop

@section ('entitybar-subtitle')

    @if ($address = $company->mainAddress)
        <i class="fa fa-map-marker"></i> {{ $address->present()->cityAndState }}
    @endif

@stop

@section ('entitybar-content') @include ('Company::Partials.show-tabs') @stop

@section ('content')

<div class="row">
    <div class="col-md-8">
        <div class="panel panel-default panel-inverse">
            <div class="panel-heading">
                Datos de empresa
                <a class="btn btn-success btn-sm" href="#"><i class="fa fa-edit"></i> Editar</a>
            </div>

            <table class="table">
                <tbody>
                    <tr>
                        <td><strong>NOMBRE</strong></td>
                        <td>{{ $company->name }}</td>
                    </tr>
                    <tr>
                        <td>test</td>
                        <td>test</td>
                    </tr>

                    @include ('Support::Entity.Show.panel-contacts', ['entity' => $company])
                </tbody>
            </table>

            <div class="panel-footer">
                <a class="btn btn-link btn-xs pull-right" href="#">Cambiar orden de aparición</a>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>


@stop