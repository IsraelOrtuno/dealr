{{-- Session/login.blade.php --}}

@extends ('Layout::login')

@section ('content')

<div class="row">

    <div class="col-md-offset-3 col-md-6">

        <p class="login-image"><img src="assets/img/login-user.jpg" class="login-image" alt=""/></p>

        <h2 class="login-title">Su Cuenta</h2>
        <p class="login-subtitle">
            @if ($errors->first())
                {{ $errors->first() }}
            @else
                Introduzca sus credenciales para acceder a su cuenta.
            @endif
        </p>

        {{ Form::open(['route' => 'authenticate', 'method' => 'post', 'class' => 'login-box']) }}
        	<div class="row">

                <div class="col-md-6 field">
                    <i class="fa fa-envelope"></i>
                    {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Dirección Email']) }}
                </div>
                <div class="col-md-6 field">
                    <i class="fa fa-lock"></i>
                    {{ Form::password('password', ['class' => 'form-control', 'placeholder' => '●●●●●']) }}
                </div>

        	</div>

            <button type="submit" class="mt10 btn btn-success btn-lg btn-block">IDENTIFICARME</button>
        {{ Form::close() }}

    </div>

</div>

@stop