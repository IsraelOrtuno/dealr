{{-- Config/layout --}}

@extends ('Layout::main')

@section ('entitybar-title') Configuración @stop

@section ('entitybar-subtitle') Configuración de la aplicación @stop

@section ('entitybar-content')

{{-- HtmlBlock::topTabs([
    link_to('#', 'Resumen'),
    link_to('#', 'General'),
    link_to('#', 'Pruebta')
]) --}}

@stop

@section ('sidebar-box')

<a class="btn btn-success btn-sm" href="{{ URL::to('/') }}">Volver a la aplicación</a>

@stop

@section ('sidebar-links')

<ul class="sidebar-nav">
    <li class="link-title">Cuenta</li>
    <li class="{{ set_active('details') }}">
        <a href="#"><i class="fa fa-dashboard fa-fw"></i> Detalles</a>
    </li>
    <li class="{{ set_active('users') }}">
        <a href="#"><i class="fa fa-cube fa-fw"></i> Usuarios</a>
    </li>

</ul>

<div class="sidebar-separator"></div>

<ul class="sidebar-nav">
    <li class="link-title">Personalización</li>
    <li>
        <a href="{{ set_active('colors') }}"><i class="fa fa-eye fa-fw"></i> Colores</a>
    </li>
    <li class="{{ set_active('config.collections') }}">
        <a href="{{ URL::route('config.collections.index') }}"><i class="fa fa-list fa-fw"></i> Colecciones</a>
    </li>
    <li class="{{ set_active('config.fields') }}">
        <a href="{{ URL::route('config.fields.index') }}"><i class="fa fa-list-alt fa-fw"></i> Campos</a>
    </li>
    <li>
        <a href="{{ set_active('tasks') }}"><i class="fa fa-bar-chart-o fa-fw"></i> Tareas/Eventos</a>
    </li>
</ul>

@stop