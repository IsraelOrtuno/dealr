{{-- Config/Collections/index --}}

@extends ('Config::layout')

@section ('entitybar-title') Personalización de Listas @stop

@section ('entitybar-subtitle') En este apartado se muestran las colecciones de elementos personalizadas. Puede crear, eliminar o editar cualquiera de ellas en función de sus necesidades. @stop

@section ('entitybar-actions')

<div class="btn-group pull-right">
    <a class="btn btn-success" href="{{ URL::route('config.collections.create') }}"><i class="fa fa-plus fa-fw"></i> Nueva colección</a>
</div>

@stop

@section ('content')

<div class="row">

    <div class="col-md-offset-2 col-md-8">

        <div class="panel panel-default panel-inverse">
            <div class="panel-heading">
                Lista de colecciones
            </div>

            <ul class="list-group">
                @foreach ($collections as $collection)

                <li class="list-group-item">{{ $collection->name }}

                    <small>({{ $collection->items->count() }} elementos)</small>

                    <div class="pull-right">
                        <a class="btn btn-success btn-xs" href="{{ URL::route('config.collections.edit', $collection->id) }}"><i class="fa fa-edit fa-fw"></i></a>
                        <a class="btn btn-xs btn-danger" href="{{ URL::route('config.collections.edit', $collection->id) }}"><i class="fa fa-trash-o fa-fw"></i></a>
                    </div>

                </li>
                @endforeach
            </ul>

            <div class="panel-footer">
                <a class="btn btn-link btn-xs pull-right" href="#">Cambiar orden de aparición</a>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>

</div>

@stop