{{-- Config/Collections/Items/create --}}

@extends ('Config::layout')

@section ('entitybar-title')
@if (isset($item))
Editar
@else
Nuevo
@endif
Elemento
@stop

@section ('entitybar-subtitle')
@if (isset($item))
Editando el elemento "{{{ $item->value }}}" de la colección <strong>{{{ $collection->name }}}</strong>.
@else
Añadiendo un nuevo elemento a la colección <strong>{{{ $collection->name }}}</strong>.
@endif
@stop

@section ('content')

<div class="row">
    <div class="col-md-offset-3 col-md-6">
        @if (!isset($item))
        {{ Form::open(['route' => ['config.collections.items.store', $collection->id], 'method' => 'post']) }}
        @else
        {{ Form::model($item, ['route' => ['config.collections.items.update', $item->id], 'method' => 'put']) }}
        @endif
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Elemento
            </div>

            <div class="panel-body">

                <div class="form-group">
                    <label for="label">Valor</label>
                    {{ Form::text('value', null, ['class' => 'form-control', 'placeholder' => 'Valor del elemento',
                    'required']) }}
                </div>

            </div>

            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Guardar</button>
                <a class="btn btn-default" href="{{ URL::route('config.collections.edit', $collection->id) }}">Volver</a>
            </div>
        </div>

    </div>
    {{ Form::close() }}

</div>

@stop