{{-- Config/Collections/create --}}

@extends ('Config::layout')

@section ('entitybar-title')
@if (isset($collection))
Editar
@else
Nueva
@endif
Colección
@stop

@section ('entitybar-actions')
@if (isset($collection))
<div class="btn-group pull-right">
    <a class="btn btn-success" href="{{ URL::route('config.collections.items.create', $collection->id) }}"><i class="fa fa-plus fa-fw"></i> Nuevo elemento</a>
</div>
@endif
@stop

@section ('entitybar-subtitle')
@if (isset($collection))
Editando la colección {{{ $collection->name }}}. Puede establecer un nombre nuevo, añadir, modificar o eliminar elementos.
@else
Creando una colección nueva. Establezca un nombre.
@endif
@stop

@section ('content')

@if ( ! isset($collection))

<div class="row">
    <div class="col-md-offset-3 col-md-6">
        {{ Form::open(['route' => 'config.collections.store', 'method' => 'post']) }}
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Nueva colección
            </div>

            <div class="panel-body">

                <div class="form-group">
                    <label for="label">Nombre</label>
                    {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre de la colección',
                    'required']) }}
                </div>

            </div>

            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Guardar</button>
                <a class="btn btn-default" href="{{ URL::route('config.collections.index') }}">Volver</a>
            </div>
        </div>

    </div>
    {{ Form::close() }}

</div>

@else

<div class="row">
    <div class="col-md-9">
        {{ Form::open(['route' => 'config.collections.update', 'method' => 'post']) }}
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Colección "{{ $collection->name }}"
            </div>

            <ul class="list-group">

                @if ( ! $collection->items->count())
                <li class="list-group-item">
                    <p>La lista se encuentra vacía, ¡considere añadir un primer elemento!</p>

                    <a class="btn btn-success" href="{{ URL::route('config.collections.items.create', $collection->id) }}"><i class="fa fa-plus fa-fw"></i> Añadir</a>
                </li>
                @endif

                @foreach ($collection->items as $item)

                <li class="list-group-item">{{ $item->value }}

                    <div class="pull-right">
                        <a class="btn btn-success btn-xs" href="{{ URL::route('config.collections.items.edit', $item->id) }}"><i class="fa fa-edit fa-fw"></i></a>
                        <a class="btn btn-xs btn-danger" href="{{ URL::route('config.collections.items.edit', $item->id) }}"><i class="fa fa-trash-o fa-fw"></i></a>
                    </div>

                </li>
                @endforeach
            </ul>

        </div>

        {{ Form::close() }}

    </div>

    <div class="col-md-3">

        <h4><i class="fa fa-exchange"></i> Nombre</h4>
        <p>Cambie el nombre de la lista desde aquí:</p>
        {{ Form::open(['route' => ['config.collections.update', $collection->id], 'method' => 'put']) }}
        <div class="form-group">
            {{ Form::text('name', $collection->name, ['class' => 'form-control', 'placeholder' => 'Nombre de la colección', 'required']) }}
        </div>
            <button type="submit" class="btn btn-primary">Renombar</button>
        {{ Form::close() }}

    </div>


</div>

@endif
@stop