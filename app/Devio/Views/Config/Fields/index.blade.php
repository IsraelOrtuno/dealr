{{-- Config/Fields/index --}}

@extends ('Config::layout')

@section ('entitybar-title') Campos Personalizados @stop

@section ('entitybar-subtitle') Ordene y personzalice los campos requerido por cada una de sus entidades. @stop

@section ('entitybar-actions')

<div class="btn-group pull-right">
    <a class="btn btn-success" href="{{ URL::route('config.fields.create') }}"><i class="fa fa-plus fa-fw"></i> Nuevo</a>
</div>

@stop

@section ('entitybar-content')

    {{ HtmlBlock::topTabs([
        link_to_route('config.fields.entity', trans('main.companies'), ['company']),
        link_to_route('config.fields.entity', trans('main.contacts'), ['contact']),
        link_to_route('config.fields.entity', trans('main.deals'), ['deal'])
    ]) }}

@stop

@section ('content')

<div class="row">

    <div class="col-md-offset-2 col-md-8">

        @if (isset($deletion))

        <div class="alert alert-danger">
            <h4>Atención</h4>
            <p>Está a punto de eliminar un campo de una entidad. Este campo puede contener valores asignados en elementos ya creados. Si lo elimina, se perderá toda la información asignada a este campo en todas las entidades. <strong>Esta acción no se puede deshacer.</strong></p>
            <p class="bold">¿Está seguro que desea continuar y eliminar el campo "{{{ $deletion->label }}}"?</p>

            {{ Form::open(['route' => ['config.fields.destroy', $deletion->id], 'method' => 'delete', 'class' => 'mt10']) }}
            <a class="btn btn-default" href="{{ URL::route('config.fields.index') }}">Cancelar</a>
            <button type="submit" class="btn btn-danger">Aceptar</button>
            {{ Form::close() }}
        </div>

        @endif

        <div class="panel panel-default panel-inverse">
            <div class="panel-heading">
                Listado de campos
            </div>

            <ul class="list-group">
                @foreach ($fields as $field)

                    <li class="list-group-item">{{ $field['label'] }}

                        <small>{{ $field['name'] }}</small>
                        @if (isset($field['id']))

                        <div class="pull-right">
                            <a class="btn btn-success btn-xs" href="{{ URL::route('config.fields.edit', $field['id']) }}"><i class="fa fa-edit fa-fw"></i></a>
                            <a class="btn btn-xs btn-danger" href="{{ URL::route('config.fields.confirm', $field['id']) }}"><i class="fa fa-trash-o fa-fw"></i></a>
                        </div>

                        @endif

                    </li>
                @endforeach
            </ul>

            <div class="panel-footer">
                <a class="btn btn-link btn-xs pull-right" href="#">Cambiar orden de aparición</a>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>

</div>

@stop