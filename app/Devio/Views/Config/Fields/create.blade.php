{{-- Config/Fields/create --}}

@extends ('Config::layout')

@section ('entitybar-title')
@if (isset($field))
Editar
@else
Nuevo
@endif
Campo Personalizado
@stop

@section ('entitybar-subtitle')
@if (isset($field))
Editando el campo {{{ $field->label }}}. No puede ser transferido a otra entidad ni cambiar su tipo.
@else
Creando un campo nuevo. Seleccione a qué entidad pertenece y el tipo de información que contendrá.
@endif
@stop

@section ('content')

<div class="row">
    <div class="col-md-offset-3 col-md-6">
        {{ Form::open(['route' => 'config.fields.store', 'method' => 'post']) }}
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Información del campo
            </div>

            <div class="panel-body">


                <div class="form-group">
                    <label for="module_type">Entidad</label>
                    {{ Form::select('module_type', $entities , null , ['class' => 'form-control', 'required']) }}
                    <p class="help-block">Entidad a la que irá asociado el campo.</p>
                </div>

                <div class="form-group">
                    <label for="label">Etiqueta</label>
                    {{ Form::text('label', null, ['class' => 'form-control', 'placeholder' => 'Etiqueta del campo',
                    'required']) }}
                </div>


                <div class="form-group">
                    <label for="name">Nombre</label>
                    {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre interno',
                    'required']) }}
                </div>

                <div class="form-group">
                    <label for="help_text">Mensaje de ayuda</label>
                    {{ Form::text('help_text', null, ['class' => 'form-control', 'required']) }}
                    <p class="help-block">Un mensaje que aparecerá debajo del campo al igual que este mismo.</p>
                </div>

                <div class="form-group">
                    <label for="type">Tipo</label>
                    {{ Form::select('type', ['' => '-- Seleccione --', 'text' => 'Texto', 'list' => 'Lista', 'integer'
                    => 'Numérico'] , null , ['class' => 'form-control', 'required']) }}
                </div>

                <div class="form-group">
                    <label for="default_value">Valor por defecto</label>
                    {{ Form::text('default_value', null, ['class' => 'form-control']) }}
                </div>


            </div>

            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Guardar</button>
                <a class="btn btn-default" href="{{ URL::route('config.fields.index') }}">Volver</a>
            </div>
        </div>

    </div>
    {{ Form::close() }}

</div>
@stop