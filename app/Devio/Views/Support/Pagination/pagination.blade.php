{{-- Support/Pagination/pagination --}}

<?php
$presenter = new Illuminate\Pagination\BootstrapPresenter($paginator);
?>

<?php if ($paginator->getLastPage() > 1): ?>
    <ul class="pagination pagination-sm">
        {{ $presenter->render() }}
    </ul>
<?php endif; ?>
