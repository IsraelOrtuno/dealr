{{-- Layout/Partials/Entity/Show/panel-contacts --}}

{{-- Importing Emails --}}
@include ('Support::Entity.Show.Contacts.email')

{{-- Importing Phones --}}
@include ('Support::Entity.Show.Contacts.phone')

{{-- Importing Addresses --}}
@include ('Support::Entity.Show.Contacts.address')