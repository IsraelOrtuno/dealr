{{-- Layout/Partials/Entity/Show/Contacts/email --}}

@if ($entity->emails)
<tr>
    <td><strong>E-MAIL</strong></td>
    <td>
        @foreach ($entity->emails as $index => $email)

        <div class="{{ ($index == (count($entity->emails) - 1)) ?: 'mb10' }}">
            <strong><i class="fa fa-envelope-o fa-fw"></i> {{ $email->name }}</strong> : {{ $email->present()->emailLink }}
        </div>

        @endforeach
    </td>
</tr>
@endif