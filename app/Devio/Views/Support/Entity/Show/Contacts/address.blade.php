{{-- Layout/Partials/Entity/Show/Contacts/address --}}

@if ($entity->addresses)
<tr>
    <td><strong>DIRECCIÓN</strong></td>
    <td>
        @foreach ($entity->addresses as $index => $address)

                <div class="{{ ($index == (count($entity->addresses) - 1)) ?: 'mb10' }}">
                    <strong><i class="fa fa-map-marker"></i> {{ $address->name }}</strong> : {{ $address->present()->fullAddress }}
                </div>

        @endforeach
    </td>
</tr>
@endif