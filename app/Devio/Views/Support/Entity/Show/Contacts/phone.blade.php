{{-- Layout/Partials/Entity/Show/Contacts/phone --}}

@if ($entity->phones)
<tr>
    <td><strong>TELÉFONO</strong></td>
    <td>
        @foreach ($entity->phones as $index => $phone)

        <div class="{{ ($index == (count($entity->phones) - 1)) ?: 'mb10' }}">
            <strong><i class="fa fa-phone fa-fw"></i> {{ $phone->name }}</strong> : {{ $phone->present()->phoneLink }}
        </div>

        @endforeach
    </td>
</tr>
@endif