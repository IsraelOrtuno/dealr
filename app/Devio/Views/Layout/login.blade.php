{{-- Layout/login --}}

<!DOCTYPE html>

<html lang="es" ng-app="app">

    <head>

        @include('Layout::Main.head')

    </head>

    <body class="login">

        <!-- #login-wrapper -->
        <section id="login-wrapper">

            <!-- #content -->
            <div id="content" class="container-fluid">
                @yield ('content')
            </div>
            <!-- /#content -->

        </section>
        <!-- /#login-wrapper -->


    </body>

</html>