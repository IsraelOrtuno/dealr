{{-- Layout/main --}}

<!DOCTYPE html>

<html lang="es" ng-app="app">

    <head>

        @include('Layout::Main.head')

    </head>

    <body>

        <!-- #wrapper -->
        <section id="wrapper">

            <!-- #toolbar -->
            @include ('Layout::Main.toolbar')
            <!-- /#toolbar -->

            <!-- #content -->
            <div id="content" class="container-fluid">
                @yield ('content')
            </div>
            <!-- /#content -->

            <!-- #footer -->
            @include('Layout::Main.footer')
            <!-- /#footer -->

        </section>
        <!-- /#wrapper -->


    </body>

</html>