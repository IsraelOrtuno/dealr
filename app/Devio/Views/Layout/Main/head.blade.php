{{-- Layout/Main/head --}}

<meta charset="utf-8">
<title> {{ APP_NAME }} - {{ APP_VERSION }} - @yield('title') </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Cream, the CRM app">
<meta name="author" content="Israel Ortuño">

<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700' rel='stylesheet' type='text/css'>

{{ HTML::style('assets/stylesheets/base.min.css', ['media' => 'all']) }}
{{ HTML::style('assets/stylesheets/main.min.css', ['media' => 'all']) }}

{{ HTML::script('assets/javascript/scripts.min.js') }}