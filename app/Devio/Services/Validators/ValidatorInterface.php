<?php namespace Devio\Services\Validators;

interface ValidatorInterface {

    /**
     * Is it valid for being stored?
     *
     * @param null $entity
     * @param null $rules
     * @return mixed
     */
    public function validForCreation($entity = null, $rules = null);

    /**
     * Is it valid for being updated?
     *
     * @param null $entity
     * @param null $rules
     * @return mixed
     */
    public function validForUpdate($entity = null, $rules = null);

    /**
     * Return the errors Array
     *
     * @return mixed
     */
    public function errors();

}