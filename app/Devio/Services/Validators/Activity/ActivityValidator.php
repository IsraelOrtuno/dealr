<?php  namespace Devio\Services\Validators\Activity;

use Devio\Services\Validators\BaseValidator;

class ActivityValidator extends BaseValidator {

    /**
     * @var array
     */
    public static $rules = [
        'activity_type_id'  => 'required|numeric',
        'subject'           => 'required',
    ];

}