<?php namespace Devio\Services\Validators\Phone;

use Devio\Services\Validators\BaseValidator;

class PhoneValidator extends BaseValidator {

    /**
     * @var array
     */
    public static $rules = [
        'name'          => 'required|min:3',
        'phone'         => 'required|min:3',
    ];

}