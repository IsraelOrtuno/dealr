<?php namespace Devio\Services\Validators\Address;

use Devio\Services\Validators\BaseValidator;

class AddressValidator extends BaseValidator {

    /**
     * @var array
     */
    public static $rules = [
        'name'          => 'required|min:3',
        'address'       => 'required|min:3',
        'post_code'     => 'required|min:5',
        'city'          => 'required|min:5',
        'state'         => 'required|min:3'
    ];

}