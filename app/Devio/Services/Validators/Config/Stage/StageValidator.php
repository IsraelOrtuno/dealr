<?php namespace Devio\Services\Validators\Config\Stages;

use Devio\Services\Validators\BaseValidator;

class StageValidator extends BaseValidator {

    /**
     * @var array
     */
    public static $rules = [
        'name'          => 'required|min:3',
        'probability'   => 'required|numeric|min:0|max:100',
    ];

}