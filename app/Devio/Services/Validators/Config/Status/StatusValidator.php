<?php namespace Devio\Services\Validators\Config\Status;

use Devio\Services\Validators\BaseValidator;

class StatusValidator extends BaseValidator {

    /**
     * @var array
     */
    public static $rules = [
        'name'          => 'required|min:3',
        'color'         => 'required'
    ];

}