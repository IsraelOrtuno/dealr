<?php  namespace Devio\Services\Validators\Config\Pipeline;

use Devio\Services\Validators\BaseValidator;

class PipelineValidator extends BaseValidator {

    /**
     * @var array
     */
    public static $rules = [
        'name'          => 'required|min:3',
        'account_id'    => 'numeric'
    ];

} 