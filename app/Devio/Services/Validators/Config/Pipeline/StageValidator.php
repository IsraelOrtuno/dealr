<?php  namespace Devio\Services\Validators\Config\Pipeline;

use Devio\Services\Validators\BaseValidator;

class StageValidator extends BaseValidator {

    /**
     * @var array
     */
    public static $rules = [
        'name'          => 'required|min:3',
        'probability'   => 'required|numeric|min:0|max:100',
        'max_idle'      => 'numeric',
        'order'         => 'numeric',
        'pipeline_id'   => 'numeric'
    ];

} 