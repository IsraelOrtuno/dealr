<?php namespace Devio\Services\Validators\Config\Source;

use Devio\Services\Validators\BaseValidator;
use Devio\Services\Validators\ValidatorInterface;

class SourceValidator extends BaseValidator {

    /**
     * @var array
     */
    public static $rules = [
        'name'          => 'required|min:3'
    ];

}