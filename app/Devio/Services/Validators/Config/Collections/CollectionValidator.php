<?php namespace Devio\Services\Validators\Config\Collections;

use Devio\Services\Validators\BaseValidator;

class CollectionValidator extends BaseValidator {

    /**
     * @var array
     */
    public static $rules = [
        'name'          => 'required|min:3'
    ];

}