<?php namespace Devio\Services\Validators\Config\Collections;

use Devio\Services\Validators\BaseValidator;

class CollectionItemValidator extends BaseValidator {

    /**
     * @var array
     */
    public static $rules = [
        'value'          => 'required|min:3'
    ];

}