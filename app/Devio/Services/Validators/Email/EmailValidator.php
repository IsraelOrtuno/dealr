<?php namespace Devio\Services\Validators\Email;

use Devio\Services\Validators\BaseValidator;

class EmailValidator extends BaseValidator {

    /**
     * @var array
     */
    public static $rules = [
        'name'          => 'required|min:3',
        'email'         => 'required|email',
    ];

}