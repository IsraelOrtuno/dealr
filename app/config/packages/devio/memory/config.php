<?php

return [

    'handler' => 'Devio\Support\TenantMemoryHandler',

    'cache'   => [
        'enabled' => true,
        // In minutes...
        'timeout' => 600
    ]

];