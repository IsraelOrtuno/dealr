<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Entity Fields
    |--------------------------------------------------------------------------
    |
    | Here are specified those entity fields which are obligatory required by
    | any of the following entities. It's the minimum field that the entity
    | needs for working properly. Feel free to add any other if required.
    |
    */

    'fields' => array(
        'company' => [
            [
                'name'  => 'name',
                'label' => trans('validation.attributes.name'),
            ],
        ],
        'contact' => [
            [
                'name'  => 'name',
                'label' => trans('validation.attributes.name'),
            ],
        ],
        'deal'    => [
            [
                'name'  => 'name',
                'label' => trans('validation.attributes.name'),
            ],
        ]
    )

);