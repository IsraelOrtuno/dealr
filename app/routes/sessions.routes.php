<?php

/*
|--------------------------------------------------------------------------
| Session Routes
|--------------------------------------------------------------------------
*/

Route::group(['namespace' => 'Devio\Controllers'], function()
{
    Route::get('login', ['as' => 'login', 'uses' => 'SessionsController@login']);

    Route::post('authenticate', ['as' => 'authenticate', 'uses' => 'SessionsController@authenticate']);

    Route::get('logout', ['as' => 'logout', 'uses' => 'SessionsController@unauthenticate']);
});

