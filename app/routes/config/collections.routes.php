<?php

/*
|--------------------------------------------------------------------------
| Config Collections
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'config', 'namespace' => 'Devio\Controllers\Config'], function ()
{
    Route::resource('collections/items', 'CollectionItemsController', ['except' => ['index', 'show', 'create', 'store']]);
    Route::get('collections/{id}/items/create', ['as' => 'config.collections.items.create', 'uses' => 'CollectionItemsController@create']);
    Route::post('collections/{id}/items', ['as' => 'config.collections.items.store', 'uses' => 'CollectionItemsController@store']);

    Route::resource('collections', 'CollectionsController', ['except' => ['show']]);

});