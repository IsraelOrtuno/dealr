<?php

/*
|--------------------------------------------------------------------------
| Config Custom Fields
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'config', 'namespace' => 'Devio\Controllers\Config'], function ()
{
    Route::get('fields/{entity}', ['as' => 'config.fields.entity', 'uses' => 'FieldsController@index'])
        ->where(['entity' => 'company|contact|deal']);

    Route::resource('fields', 'FieldsController');

    Route::get('fields/{id}/confirm', [
        'as' => 'config.fields.confirm',
        'uses' => 'FieldsController@confirmDeletion']
    );



});