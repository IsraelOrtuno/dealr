<?php

/*
|--------------------------------------------------------------------------
| Config Routes
|--------------------------------------------------------------------------
*/

Route::get('config', ['as' => 'config', 'before' => 'auth', 'uses' => 'Devio\Controllers\Config\ConfigController@index']);