<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Main App Language Lines
    |--------------------------------------------------------------------------
    */

    'dashboard'     => 'escritorio',

    'entity'        => 'entidad',
    'entities'      => 'entidades',

    'organization'  => 'organización',
    'organizations' => 'organizaciones',

    'company'       => 'empresa',
    'companies'     => 'empresas',

    'contact'       => 'contacto',
    'contacts'      => 'contactos',

    'deal'          => 'operación',
    'deals'         => 'operaciones',

    'task'          => 'tarea',
    'tasks'         => 'tareas',

    'note'          => 'nota',
    'notes'         => 'notas',

    'file'          => 'archivo',
    'files'         => 'archivos',

    'report'        => 'informe',
    'reports'       => 'informes'

);
