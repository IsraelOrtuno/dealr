<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Just loading every route file located into the "app/routes" directory.
|
*/

foreach (File::allFiles(__DIR__.'/routes') as $partial)
{
    require_once $partial->getPathName();
}