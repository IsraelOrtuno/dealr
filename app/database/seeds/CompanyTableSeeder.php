<?php

class CompanyTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 100; $i ++)
        {
            $name = $faker->company;

            if (($i % 2) == 0)
            {
                $name .= " {$faker->companySuffix}";
            }

            $company = \Devio\Entities\Company\Company::create(
                [
                    'name'        => $faker->company,
                    'description' => $faker->paragraph(rand(0, 3)),
                    'account_id'  => 1
                ]
            );
        }
    }

} 