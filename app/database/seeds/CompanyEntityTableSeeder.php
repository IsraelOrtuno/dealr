<?php

use Faker\Factory as Faker;
use Devio\Entities\Phone\Phone;
use Devio\Entities\Email\Email;
use Devio\Entities\Address\Address;

class CompanyEntityTableSeeder extends Seeder {

    /**
     *
     */
    public function run()
    {
        $faker = Faker::create();

        $companies = Devio\Entities\Company\Company::all();

        foreach ($companies as $company)
        {
            // Address
            foreach (range(1, mt_rand(1, 3)) as $index)
            {
                $address = new Address();
                $address->fill([
                                   'name'      => ucfirst($faker->word),
                                   'state'     => ucfirst($faker->word),
                                   'country'   => ucfirst($faker->country),
                                   'city'      => ucfirst($faker->city),
                                   'post_code' => $faker->postcode,
                                   'address'   => $faker->address
                               ]);

                $company->addresses()->save($address);
            }

            // Email
            foreach (range(1, mt_rand(1, 3)) as $index)
            {
                $email = new Email();

                $email->fill([
                                 'name'  => ucfirst($faker->word),
                                 'email' => $faker->companyEmail
                             ]);

                $company->emails()->save($email);
            }

            // Phone
            foreach (range(1, mt_rand(1, 3)) as $index)
            {
                $phone = new Phone();

                $phone->fill([
                                 'name'  => ucfirst($faker->word),
                                 'phone' => $faker->phoneNumber
                             ]);

                $company->phones()->save($phone);
            }

        }
    }

}