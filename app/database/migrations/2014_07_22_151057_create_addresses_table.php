<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAddressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('addresses', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name');
			$table->string('address');
			$table->string('state');
			$table->string('city');
			$table->string('post_code', 5);
            $table->string('country');
            $table->integer('order');

            // Polymorphic relationship
            $table->integer('addressable_id')->unsigned();
            $table->string('addressable_type', 32);

            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('addresses');
	}

}
