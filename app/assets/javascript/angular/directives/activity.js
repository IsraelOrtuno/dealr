app.directive("activity", function($timeout) {

    // URL we are making the request to
    var createUrl  = window.location.origin + '/activity/';

    return {
        restrict: 'A',
        scope: true,
        controller: ['$scope', '$http', '$timeout', '$parse', function($scope, $http, $timeout, $parse)
        {
            // Initialize the activity data object
            $scope.activity = {
                subject:          null,
                activity_type_id: null,
                description:      null,
                date:             null,
                time:             null,
                duration:         null,
                set_as_done:      false,
                attaches: {
                    company:  null,
                    people:   null,
                    deal:     null
                }
            };

            $scope.save = function()
            {
                $http.post(createUrl, { data: $scope.activity })
                    // Went OK, pass data to results variable
                    .success(function (data, status) {
                        console.log('ok');
                    })
                    // Anything went wrong, just alert the user...
                    .error(function (data, status) {
                        alert('Ha fallado la validación de la parte del servidor.');
                    });
            };

            // Attaches an element to the activity array
            this.attachElement = function(elementType, element)
            {
                var getter = $parse('attaches.' + elementType);
                var setter = getter.assign;

                // New element
                if ((typeof element == 'string') || (element instanceof String))
                    setter($scope.activity, { name: element });
                // It's got id, element does exist
                else
                    setter($scope.activity, { id: element.id });
            };


            // Dettaching an element from the activity array
            this.dettachElement = function(elementType)
            {
                var getter = $parse('attaches.' + elementType);
                var setter = getter.assign;
                setter($scope.activity, null);
            };

            return;
        }]
    };
});

//$data = [
//    'activity_type_id' => 1,
//    'assigned_to' => 1,
//    'subject' => 'Asunto de prueba',
//    'description' => 'Descripción de prueba',
//    'duration' => 15,
//    'attaches' => [
//        'company' => ['id' => 1],
//        'people' => ['name' => 'Israel Peidro']
//    ]
//];