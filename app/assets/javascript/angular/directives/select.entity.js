app.directive("selectEntity", function($timeout) {

    // Global var that stores every timeout in order to cancel them typing fast
    var $prevTimeout;

    return {
        restrict: 'A',
        replace: true,
        require: '?^activity',      // Requiring activity directive (^) and passing null if not found (?)
        scope: {
            label: '@',             // Fields label name
            type: '@',              // Entity type to work with
            initialElement: '@'     // If any element id is selected by default
        },
        templateUrl: window.location.origin + '/assets/templates/select.entity.template.html',
        controller: ['$scope', '$http', '$timeout', '$sce', function($scope, $http, $timeout, $sce)
        {
            // URL based on type
            var searchUrl   = window.location.origin + '/search/' + $scope.type;
            var getUrl      = window.location.origin + '/search/get/' + $scope.type;
            // Dropdown jQuery entity
            var dropdown = $('#' + $scope.type + 'Suggestions');

            // Watching the input for any changes
            $scope.$watch('element', function(newValue) {
                $scope.newElement = null;

                // Fixes bug, first checking if newValue exists of any type.
                if (( typeof newValue === "undefined") || ( ! newValue))
                    return;

                // Cancelling previous timer
                $timeout.cancel($prevTimeout);

                // If the input has more than 2 characters then...
                if (newValue.length > 2)
                {
                    // Cancelling previous timer
//                    $timeout.cancel($prevTimeout);

                    // Timeout before performing the search just in case any other letter comes in
                    $prevTimeout = $timeout(function() {
                        $scope.search();
                    }, 500);
                }
                // Otherwise, just clear every result and close the dropdown
                else
                {
                    // console.log($scope.selected);
                    $scope.results = {};
                    if (!$scope.selected)
                        $scope.dettachFromParent();
                }
            });

            // Search for the entities that match
            $scope.search = function()
            {
                // Performing the POST query
                $http.post(searchUrl, { query: $scope.element })
                    // Went OK, pass data to results variable
                    .success(function (data, status) {
                        if (data.length < 1)
                        {
                            $scope.deselect();
                            $scope.notifyNewCreation();
                        }
                        else
                        {
                            $scope.results = data;
                            $('#' + $scope.type + 'Suggestions').dropdown('toggle');
                        }
                    })
                    // Anything went wrong, just alert the user...
                    .error(function (data, status) {
                        alert('Se ha producido un error');
                    });
            }

            // Selects an element if initial-element tag has been set to any ID
            $scope.initializeWithElement = function()
            {
                if ( ! $scope.initialElement)
                    return;

                $http.post(getUrl, { query: $scope.initialElement })
                    // Went OK, retrieve and select element
                    .success(function(data, status) {
                        if (data)
                            $scope.select(data);
                    })
                    // Element wasn't found...
                    .error(function(data, status) {
                        alert('Se ha producido un error recuperando el objeto.')
                    });
            }

            // An element has been selected
            $scope.select = function(element)
            {
                $scope.element = element.id;
                $scope.selected = element;
                $scope.results = {};
                $scope.updateParentData();
            }

            // Deselect the current selected element
            $scope.deselect = function(clean)
            {
                $scope.selected = null;

                if (clean)
                    $scope.element = null;

                $scope.dettachFromParent();
            }

            // OnBlur (lost focus)
            $scope.hide = function($event)
            {
                // Doing inside the timeout because it fires the blur event before the click.
                // This was causing problems as it was loosing the focus when an element was being selected
                // and was not allowing to select any item.
                $timeout(function()
                {
                    $scope.results = {};

                    // Fixes bug, if no element typed into the input box, do not check its length...
                    if ((typeof $scope.element === "undefined") || ( ! $scope.element))
                        return;

                    // If no element is selected and it's longer than 3 chars, let's notificate the user that
                    // a new element is going to be created.
                    if (($scope.element.length > 2) && ( ! $scope.selected))
                    {
                        $scope.notifyNewCreation();est
                        $scope.updateParentData();
                    }

                }, 100)

            }

            // Adds a message that notifies a new element will be created.
            $scope.notifyNewCreation = function()
            {
                if ($scope.type != 'deal')
                    $scope.newElement = $sce.trustAsHtml('<i class="fa fa-long-arrow-up fa-fw"></i> Se creará como nuevo elemento.');
                else
                    $scope.newElement = $sce.trustAsHtml('<i class="fa fa-warning fa-fw"></i> No se ha seleccionado una operación.');
            }

            // Sends the selected entity values to the parent
            $scope.updateParentData = function()
            {
                if ($scope.activityCtrl)
                {
                    $scope.activityCtrl.attachElement($scope.type, $scope.selected || $scope.element);
                }
            }

            // Removes the selected entity from the parent
            $scope.dettachFromParent = function()
            {
                if ($scope.activityCtrl)
                {
                    $scope.activityCtrl.dettachElement($scope.type);
                }
            }

            // If initial element, load and select it
            $scope.initializeWithElement();

            return;
        }],

        // Initialiting the inherited controller "activity".
        link: function(scope, element, attrs, activityCtrl) {
            if (activityCtrl)
                scope.activityCtrl = activityCtrl;
        }
    };
});