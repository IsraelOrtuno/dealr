app.controller('contactController', function($scope, $http, Contact) {

    var info = {};
    var $target;
    $scope.elementData = {};
    $scope.errors = {};

    $scope.initialize = function(contactType, entity, id, targetId)
    {
        info.contactType = contactType;
        info.entity      = entity;
        info.id          = id;

        $target = $('#'+targetId);

        $scope.loadElements();
    }

    $scope.loadElements = function()
    {
        Contact.get(info)
            .success(function(data) {
                $scope.elements    = data;
            });
    }

    $scope.createSubmit = function()
    {
        Contact.save(info, $scope.elementData)
            .success(function(data) {
                $scope.loadElements();

                $target.collapse('hide');
                $scope.elementData = {};
                $scope.errors = {};
            })
            .error(function(data) {
                $scope.errors = data.errors;
            });
    };

    $scope.updateSubmit = function(element, target)
    {
        Contact.update(element, info.contactType)
            .success(function(data) {
                $('#' + target + element.id).collapse('hide');
            })
            .error(function(data) {
                element.errors = data.errors;
            });
    }

    $scope.delete = function(element, target)
    {
        Contact.delete(element, info.contactType)
            .success(function(data) {
                $('#' + target + element.id).slideUp();
            })
            .error(function(data) {
                alert(data);
            });
    }

});
