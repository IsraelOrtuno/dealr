// Changing AngularJS {{ curly brackets }} for [[ custom brackets ]]. This
// way it can be mixed with blade templating nomenclature.
// Catching also every http event and showing the spinner everytime's caught.
var app = angular.module('app', ['ngResource']);

app.config(['$interpolateProvider', function ($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
}])
.filter('isEmpty', function() {
    return function(input, replaceText) {
        if (input == null)
            return replaceText;
        if ( ! input)
            return replaceText;

        return input;
    }
});