app.factory('Contact', function($http) {

    return {
        // get all the addresses
        get : function(info) {
            return $http.get('/api/' + info.contactType + '/' + info.entity + '/' + info.id);
        },

        // save an address
        save : function(info, elementData) {
            return $http({
                method  : 'POST',
                url     : '/api/' + info.contactType + '/' + info.entity + '/' + info.id,
                headers : { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data    : $.param(elementData)
            });
        },

        // update an address
        update : function(elementData, element) {
            return $http({
                method : 'PUT',
                url     : '/api/' + element,
                headers : { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data    : $.param(elementData)
            });
        },

        // delete an address
        delete: function(elementData, element) {
            return $http({
                method: 'DELETE',
                url: '/api/' + element,
                headers : { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data    : $.param(elementData)
            });
        }
    }
});
