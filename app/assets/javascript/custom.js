
jQuery(window).load(function() {
   
   // Page Preloader
   // jQuery('#status').fadeOut();
   // jQuery('#preloader').delay(350).fadeOut(function(){
   //    jQuery('body').delay(350).css({'overflow':'visible'});
   // });
});

jQuery(document).ready(function() {
   
   // Toggle Left Menu
   jQuery('.nav-parent > a').click(function() {
      
      var parent = jQuery(this).parent();
      var sub = parent.find('> ul');
      
      // Dropdown works only when leftpanel is not collapsed
      if(!jQuery('body').hasClass('leftpanel-collapsed')) {
         if(sub.is(':visible')) {
            sub.slideUp(200, function(){
               parent.removeClass('nav-active');
               jQuery('.mainpanel').css({height: ''});
               adjustmainpanelheight();
            });
         } else {
            closeVisibleSubMenu();
            parent.addClass('nav-active');
            sub.slideDown(200, function(){
               adjustmainpanelheight();
            });
         }
      }
      return false;
   });
   
   function closeVisibleSubMenu() {
      jQuery('.nav-parent').each(function() {
         var t = jQuery(this);
         if(t.hasClass('nav-active')) {
            t.find('> ul').slideUp(200, function(){
               t.removeClass('nav-active');
            });
         }
      });
   }
   
   function adjustmainpanelheight() {
      // Adjust mainpanel height
      var docHeight = jQuery(document).height();
      if(docHeight > jQuery('.mainpanel').height())
         jQuery('.mainpanel').height(docHeight);
   }
   
   // Minimize Button in Panels
   jQuery('.minimize').click(function(){
      var t = jQuery(this);
      var p = t.closest('.panel');
      if(!jQuery(this).hasClass('maximize')) {
         p.find('.panel-body, .panel-footer').slideUp(200);
         t.addClass('maximize');
         t.html('&plus;');
      } else {
         p.find('.panel-body, .panel-footer').slideDown(200);
         t.removeClass('maximize');
         t.html('&minus;');
      }
      return false;
   });

   // Add class everytime a mouse pointer hover over it
   jQuery('.nav-cream > li').hover(function(){
      jQuery(this).addClass('nav-hover');
   }, function(){
      jQuery(this).removeClass('nav-hover');
   });

    jQuery('.sortable').sortable({
            handle : 'i',
            update : function(event, ui)
            {
                var info = $(this).sortable("serialize");

                $.ajax({
                    type:   "PUT",
                    url:    document.URL + '/reorder',
                    data:   info,

                });
            }
        }
    );

    // Clockpicker
    jQuery('.clockpicker').clockpicker({
        donetext: 'Aceptar',
        autoclose: true
    });

    // Datepicker
    jQuery('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        weekStart: 1,
        todayBtn: "linked",
        language: "es",
        orientation: "top auto",
        calendarWeeks: true,
        todayHighlight: true,
        autoclose: true
    });

    // Editable fields with a WYSIWYG
    $(function() {
        $('.selector').editable({
            // Set custom buttons with separator between them.
            buttons: ["undo", "redo", "sep", "bold", "italic", "underline", "sep", "insertOrderedList", "insertUnorderedList", "sep", "createLink"],
            inverseSkin: true,
            inlineMode: false,
            preloaderSrc: window.location.origin + 'assets/image/preloader.gif',
            contentChangedCallback: function () {
                var $this = this;

                // Altering the model manually as there was no way to update it automatically
                var scope = angular.element(".selector").scope();
                scope.$apply(function(){
                    scope.activity.description = $this.getHTML();
                });
            }
        })
    });

    // Menu Toggle
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("active");
    });
   
   
   // Menu Toggle
//   jQuery('.menutoggle').click(function(){
//
//      var body = jQuery('body');
//      var bodypos = body.css('position');
//
//      if(bodypos != 'relative') {
//
//         if(!body.hasClass('leftpanel-collapsed')) {
//            body.addClass('leftpanel-collapsed');
//            jQuery('.nav-cream ul').attr('style','');
//
//            jQuery(this).addClass('menu-collapsed');
//
//         } else {
//            body.removeClass('leftpanel-collapsed chat-view');
//            jQuery('.nav-cream li.active ul').css({display: 'block'});
//
//            jQuery(this).removeClass('menu-collapsed');
//
//         }
//      } else {
//
//         if(body.hasClass('leftpanel-show'))
//            body.removeClass('leftpanel-show');
//         else
//            body.addClass('leftpanel-show');
//
//         adjustmainpanelheight();
//      }
//
//   });
   
//   reposition_searchform();
   
//   jQuery(window).resize(function(){
//
//      if(jQuery('body').css('position') == 'relative') {
//
//         jQuery('body').removeClass('leftpanel-collapsed chat-view');
//
//      } else {
//
//         jQuery('body').removeClass('chat-relative-view');
//         jQuery('body').css({left: '', marginRight: ''});
//      }
//
//      reposition_searchform();
//
//   });
   
//   function reposition_searchform() {
//      if(jQuery('.searchform').css('position') == 'relative') {
//         jQuery('.searchform').insertBefore('.leftpanelinner .userlogged');
//      } else {
//         jQuery('.searchform').insertBefore('.header-right');
//      }
//   }

   // // Sticky Header
   // if(jQuery.cookie('sticky-header'))
   //    jQuery('body').addClass('stickyheader');
      
   // // Sticky Left Panel
   // if(jQuery.cookie('sticky-leftpanel')) {
   //    jQuery('body').addClass('stickyheader');
   //    jQuery('.leftpanel').addClass('sticky-leftpanel');
   // }
   
   // // Left Panel Collapsed
   // if(jQuery.cookie('leftpanel-collapsed')) {
   //    jQuery('body').addClass('leftpanel-collapsed');
   //    jQuery('.menutoggle').addClass('menu-collapsed');
   // }
   
   // Check if leftpanel is collapsed
//   if(jQuery('body').hasClass('leftpanel-collapsed'))
//      jQuery('.nav-cream .children').css({display: ''});

});