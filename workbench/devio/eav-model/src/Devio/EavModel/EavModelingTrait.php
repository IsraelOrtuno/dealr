<?php namespace Devio\EavModel;

use Devio\EavModel\Models\EavField;

trait EavModelingTrait {

    /**
     * @var bool
     */
    private $eavBooted = false;

    /**
     * @var array
     */
    protected $eavAttributes = [];

    /**
     * Relationship to the fields table
     *
     * @return EavRelationship
     */
    public function fields()
    {
        $instance = new EavField();

        return new EavRelationship($instance->newQuery(), $this, $this->getMorphClass());
    }

    /**
     * @return mixed
     */
    public function values()
    {
        return $this->morphMany('Devio\EavModel\Models\EavValue', 'entity');
    }

    /**
     *
     */
    private function bootEavIfNotBooted()
    {
        if ($this->eavBooted)
            return;

        $this->fillAttributes();

        $this->eavBooted = true;
    }

    /**
     *
     */
    protected function fillAttributes()
    {
        foreach ($this->fields as $field)
        {
            if ($value = $this->values->find($field->eav_field_id))
            {
                $this->eavAttributes[$field->name] = $value->value;
            }
        }
    }

    /**
     * @param array $options
     */
    public function save(array $options = [])
    {
        return parent::save($options);
    }

    /**
     * Checks if the field exists.
     *
     * @param $field
     *
     * @return bool
     */
    protected function isField($field)
    {
        return in_array($field, $this->fields->lists('name'));
    }

    /**
     * @param $value
     */
    protected function valueExists($value)
    {

    }

    /**
     * @param $key
     */
    protected function findValueItem($key)
    {

    }

    /**
     * @param $key
     * @param $value
     */
    public function setEavAttribute($key, $value)
    {
        $value = $this->findValueItem($key);
        $this->eavAttributes[$key] = $value;
    }

    /**
     * @param $key
     * @param $value
     *
     * @return bool
     */
    function __set($key, $value)
    {
        $this->bootEavIfNotBooted();

        if ($this->isField($key))
        {
            $this->setEavAttribute($key, $value);

            return;
        }

        return parent::__set($key, $value);
    }

    /**
     * Determine if an attribute exists on the EAV model.
     *
     * @param  string  $key
     * @return void
     */
    public function __isset($key)
    {
//        return (isset($this->eavAttributes[$key]) || parent::__isset($key));
    }

    /**
     * Unset an attribute on the model.
     *
     * @param  string  $key
     * @return void
     */
    public function __unset($key)
    {
//        unset($this->eavAttributes[$key]);
//
//        parent::__unset($key);
    }

}