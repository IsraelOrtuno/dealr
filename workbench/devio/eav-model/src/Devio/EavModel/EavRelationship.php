<?php namespace Devio\EavModel;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

class EavRelationship extends HasMany {

    /**
     * @var
     */
    protected $entity;

    public function __construct(Builder $query, Model $model, $entity)
    {
        $this->entity = $entity;

        parent::__construct($query, $model, 'entity_type', '');
    }

    /**
     * Set the base constraints on the relation query.
     *
     * @return void
     */
    public function addConstraints()
    {
        $this->query->where($this->getForeignKey(), '=', $this->entity)->where('account_id', '=', 1);
    }

    /**
     * Set the constraints for an eager load of the relation.
     *
     * @param  array $models
     *
     * @return void
     */
    public function addEagerConstraints(array $models)
    {
        return $this->addConstraints();
    }

    /**
     * Match the eagerly loaded results to their many parents.
     *
     * @param  array                                    $models
     * @param  \Illuminate\Database\Eloquent\Collection $results
     * @param  string                                   $relation
     * @param  string                                   $type
     *
     * @return array
     */
    protected function matchOneOrMany(array $models, Collection $results, $relation, $type)
    {
        foreach ($models as $model)
        {
            $model->setRelation($relation, $results);
        }

        return $models;
    }

    /**
     * Get the key value of the paren's local key.
     *
     * @return mixed
     */
    public function getParentKey()
    {
        return $this->entity;
    }

}