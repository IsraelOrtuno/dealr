<?php namespace Devio\EavModel\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class EavField extends Eloquent {

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * Relationship between the field and the value.
     *
     * @return mixed
     */
    public function value()
    {
        return $this->hasMany('Devio\EavModel\Models\EavValue');
    }

}