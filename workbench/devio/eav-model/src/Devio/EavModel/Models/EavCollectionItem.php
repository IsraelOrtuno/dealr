<?php namespace Devio\EavModel\Models; 

use Illuminate\Database\Eloquent\Model as Eloquent;

class EavCollectionItem extends Eloquent {

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * Relationship to the collection the item belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function collection()
    {
        return $this->belongsTo('Devio\EavModel\Models\EavCollection', 'eav_collection_id');
    }

} 