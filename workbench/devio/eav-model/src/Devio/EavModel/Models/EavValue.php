<?php namespace Devio\EavModel\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class EavValue extends Eloquent {

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * Relationship to the field table.
     *
     * @return mixed
     */
    public function field()
    {
        return $this->belongsTo('Devio\EavModel\Models\EavField');
    }

    public function entity()
    {
        return $this->morphTo();
    }

} 