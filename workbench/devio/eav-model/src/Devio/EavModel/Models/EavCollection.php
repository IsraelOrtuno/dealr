<?php namespace Devio\EavModel\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class EavCollection extends Eloquent {

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * Relationship to the table items.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('Devio\EavModel\Models\EavCollectionItem', 'eav_collection_id');
    }

    /**
     * Add a new item to the collection.
     *
     * @param $value
     *
     * @return Eloquent
     */
    public function add($value)
    {
        return $this->items()->create(compact('value'));
    }

} 