<?php

return [

    'handler' => 'Devio\Memory\MemoryHandler',

    'cache' => [
        'enabled' => true,
        // In minutes...
        'timeout' => 600
    ]

];