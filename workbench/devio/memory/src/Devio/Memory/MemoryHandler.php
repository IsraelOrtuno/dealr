<?php namespace Devio\Memory;

use Illuminate\Support\Arr;
use Devio\Memory\Facade\Memory;
use Illuminate\Cache\Repository;
use Illuminate\Support\Facades\Cache;

class MemoryHandler {

    /**
     * @var array
     */
    public $items = [];

    /**
     * @var array
     */
    protected $keyMap = [];

    /**
     * @var array
     */
    protected $cacheConfig;

    /**
     * @var string
     */
    protected $cacheKey = 'memory-db';


    public function __construct(array $cacheConfig)
    {
        $this->cacheConfig = $cacheConfig;
    }

    /**
     *
     */
    public function initiate()
    {
        // Get the resolver query, this method might be
        // overwritten if required and execute a different query.
        $query = $this->resolver();

        if ($this->cacheConfig['enabled'])
            $query->remember($this->cacheConfig['timeout'], $this->cacheKey);

        $memories = $query->get();
        $items = [];

        foreach ($memories as $memory)
        {
            // Add every memory item into the main items array
            $items = Arr::add($items, $memory->name, unserialize($memory->value));

            // Creates a key to check when saving if the item has changed
            $this->addKey($memory->name, array(
                'id'    => $memory->id,
                'value' => $memory->value,
            ));
        }

        $this->items = $items;
    }

    /**
     *
     */
    public function finish()
    {
        $changed = false;

        foreach ($this->items as $key => $value)
        {
            $isNew = $this->isNewKey($key);
            $value = serialize($value);

            if ( ! $this->check($key, $value))
            {
                $changed = true;

                $this->save($key, $value, $isNew);
            }
        }

        if ($changed && $this->cacheConfig['enabled'])
        {
            Cache::forget($this->cacheKey);
        }

//        if ($changed && $this->cache instanceof Repository) {
//            $this->cache->forget($this->cacheKey);
//        }

        return true;
    }

    /**
     * @return mixed
     */
    protected function resolver()
    {
        return Memory::newInstance();
    }

    /**
     * Add key with id and checksum.
     *
     * @param  string $name
     * @param  array  $option
     *
     * @return void
     */
    protected function addKey($name, $option)
    {
        $option['checksum'] = $this->generateNewChecksum($option['value']);
        unset($option['value']);

        $this->keyMap = Arr::add($this->keyMap, $name, $option);
    }

    /**
     * Verify checksum.
     *
     * @param  string $name
     * @param  string $check
     *
     * @return boolean
     */
    protected function check($name, $check = '')
    {
        return (Arr::get($this->keyMap, "{$name}.checksum") === $this->generateNewChecksum($check));
    }

    /**
     * Generate a checksum from given value.
     *
     * @param $value
     *
     * @return string
     */
    protected function generateNewChecksum($value)
    {
        ! is_string($value) && $value = (is_object($value) ? spl_object_hash($value) : serialize($value));

        return md5($value);
    }

    /**
     * Is given key a new content.
     *
     * @param  string $name
     *
     * @return integer
     */
    protected function getKeyId($name)
    {
        return Arr::get($this->keyMap, "{$name}.id");
    }

    /**
     * Get if from content is new.
     *
     * @param  string $name
     *
     * @return boolean
     */
    protected function isNewKey($name)
    {
        return is_null($this->getKeyId($name));
    }

    /**
     * Create/insert data to database.
     *
     * @param  string $key
     * @param  mixed  $value
     * @param  bool   $isNew
     *
     * @return bool
     */
    protected function save($key, $value, $isNew = false)
    {
        $model = Memory::where('name', '=', $key)->first();

        if (true === $isNew && is_null($model))
        {
            Memory::create(array(
                               'name'  => $key,
                               'value' => $value,
                           ));
        } else
        {
            $model->value = $value;

            $model->save();
        }
    }

    /**
     * @param $key
     * @param $value
     *
     * @return mixed
     */
    public function put($key, $value)
    {
        $this->items = Arr::set($this->items, $key, $value);

        return $value;
    }

    /**
     * @param      $key
     * @param null $default
     *
     * @return mixed
     */
    public function get($key, $default = null)
    {
        $value = Arr::get($this->items, $key, $default);

        if (is_null($value))
        {
            return value($default);
        }

        return $value;
    }

    /**
     * @param $key
     *
     * @return bool
     */
    public function has($key)
    {
        return ! is_null($this->get($key));
    }

    /**
     * @param $key
     */
    public function forget($key)
    {
        Arr::forget($this->items, $key);
    }

    /**
     * @return array
     */
    public function all()
    {
        return $this->items;
    }

} 