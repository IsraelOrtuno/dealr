<?php namespace Devio\Memory;

use Illuminate\Cache\Repository;
use Illuminate\Support\ServiceProvider;

class MemoryServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->package('devio/memory', 'devio/memory');

        $this->registerMemoryEvent();
    }

    /**
     * Register memory events during booting. This method will
     * save all changes once request has finished.
     */
    protected function registerMemoryEvent()
    {
        $app = $this->app;

        // Initiate the memory
        $this->app->before(function () use ($app)
        {
            $app['devio.memory']->initiate();
        });

        // Finish the memory
        $this->app->after(function () use ($app)
        {
            $app['devio.memory']->finish();
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $app = $this->app;

        $this->app->bindShared('devio.memory', function () use ($app)
        {
            $cacheConfig = $app['config']->get('devio/memory::cache');
            $className = $app['config']->get('devio/memory::handler');

            return new $className($cacheConfig);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('devio.memory');
    }

}
