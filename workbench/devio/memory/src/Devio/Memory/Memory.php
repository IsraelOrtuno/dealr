<?php namespace Devio\Memory;

class Memory extends \Eloquent {

    /**
     * @var string
     */
    public $table = 'settings';

    /**
     * @var array
     */
    public $guarded = [];

    /**
     * @var bool
     */
    public $timestamps = false;

}