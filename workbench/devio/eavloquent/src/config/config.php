<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tablenames
    |--------------------------------------------------------------------------
    */

    'tables' => [
        'entity_fields_value' => 'entity_fields_value',
        'entity_fields_meta'  => 'entity_fields_meta'
    ]

];