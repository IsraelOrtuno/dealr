<?php namespace Devio\Eavloquent; 

use Illuminate\Support\Facades\Config;

abstract class EntityFieldsBase extends \Eloquent {

    /**
     * Table name value, making it public
     *
     * @var string
     */
    public $table;

    /**
     * @var array
     */
    protected $guarded = array();

    public function __construct(array $attributes = array())
    {
        $this->setTableName();

        parent::__construct($attributes);
    }

    /**
     * Sets table name to the $table property
     */
    protected function setTableName()
    {
        $propertyName = snake_case(class_basename(get_class($this)));

        $tableName = Config::get("eavloquent::tables.{$propertyName}");

        $this->table = $tableName;
    }

}