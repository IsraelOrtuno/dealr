<?php namespace Devio\Eavloquent;

class EntityFieldsValue extends EntityFieldsBase {

    /**
     * @return mixed
     */
    public function meta()
    {
        return $this->belongsTo('Devio\Eavloquent\EntityFieldsMeta', 'entity_fields_meta_id');
    }

}