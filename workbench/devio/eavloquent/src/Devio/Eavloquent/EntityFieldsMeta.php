<?php namespace Devio\Eavloquent;

class EntityFieldsMeta extends EntityFieldsBase {

    /**
     * @return mixed
     */
    public function value()
    {
        return $this->hasOne('Devio\Eavloquent\EntityFieldsValue', 'entity_fields_meta_id');
    }

}