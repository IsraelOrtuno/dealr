<?php namespace Devio\Eavloquent;

use Illuminate\Support\Collection;

class Eavloquent extends \Eloquent {

    /**
     * Name of the current model module.
     *
     * @var string
     */
    protected $entityModule;

    /**
     * Relationship to the entity meta fields table.
     *
     * @var
     */
    protected $entityFields;

    /**
     * Relationship to the value fields table.
     *
     * @var
     */
    protected $entityValues;

    /**
     * Collection of the custom EAV attributes
     *
     * @var array
     */
    protected $eavAttributes = array();

    /**
     * @param array $attributes
     */
    function __construct(array $attributes = array())
    {
        // Setting the entityModule to the current model (ex: Lion\Company)
        $this->entityModule = $this->getMorphClass();

        parent::__construct($attributes);
    }

    public function fields()
    {
        $foreignKey = $this->entityModule;

        $foreignKey = 'module_type';

        $instance = new EntityFieldsMeta();

        return new EavOneOrMany($instance->newQuery(), $this, $instance->getTable().'.'.$foreignKey, '');
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function newFromBuilder($attributes = array())
    {
        $instance = parent::newFromBuilder($attributes);

        $instance->loadEavAttributes();

        return $instance;
    }

    /**
     * Returns a collection of the custom fields. Ej: Model::fieldsList();
     */
    public static function fieldsList()
    {
        $instance = new static;

        return $instance->entityFields();
    }

    /**
     * Save the parent model and its EAV depending rows.
     *
     * @param array $options
     */
    public function save(array $options = array())
    {
        // Save the parent entity first
        $parentSave = parent::save($options);

        // Saving every attribute row
        foreach ($this->eavAttributes as $attr)
        {
            // If any attribute has been left empty or NULL, it'll be just deleted from the database
            if (empty($attr->value))
                $attr->delete();
            else
                $attr->save();
        }

        return $parentSave;
    }

    /**
     * Fills the parent attributes but also fills the EAV custom fields.
     *
     * @param array $attributes
     */
    public function fill(array $attributes)
    {
        foreach ($attributes as $key => $value)
        {
            if ($this->eavFieldExists($key))
                $this->setEavAttribute($key, $value);
        }

        return parent::fill($attributes);
    }

    /**
     * Return a relationship to the EntityFieldsMeta table.
     *
     * @return mixed
     */
    public function entityFields()
    {
        return $this->fields();

//        if ( ! $this->entityFields)
//            $this->entityFields = EntityFieldsMeta::where('module_type', $this->entityModule)
//                                   ->where('account_id', 1)
//                                   ->get();
//
//        return $this->entityFields;
    }

    /**
     * Returns a relationship to the EntityFieldsValue table.
     *
     * @return mixed
     */
    public function entityValues()
    {
        if ( ! $this->entityValues)
            $this->entityValues = EntityFieldsValue::where('module_type', $this->entityModule)
                                                   ->where('module_id', $this->id)
                                                   ->with('meta')
                                                   ->get();

        return $this->entityValues;
    }

    /**
     * Determines if a field exists in the main EAV table.
     *
     * @param $key
     * @return bool
     */
    public function eavFieldExists($key)
    {
        // Filters the fields collection looking for the field. The new collection
        // is stored in this variable
        $collection = $this->entityFields()->filter(function($item) use ($key) {
            if ($item->name == $key)
                return true;
        });

        // If any item is found, return true, otherwise, false
        return $collection->count() ? true : false;
    }

    /**
     * Adds the EAV field and values to
     */
    public function loadEavAttributes()
    {
        $this->eavAttributes = $this->getEavAttributesCollection();
    }

    /**
     * Returns a collection
     *
     * @return Collection
     */
    public function getEavAttributesCollection()
    {
        $fields = $this->entityFields();

        $col = new Collection();

        foreach ($fields as $field)
        {
            $col->put($field->name, $this->getEavValueElement($field->id));
        }

        return $col;
    }

    /**
     * Return the EAV value element related that matches the fieldMetaId
     * passed by argument.
     *
     * @param $fieldMetaId
     * @return EntityFieldsValue
     */
    public function getEavValueElement($fieldMetaId)
    {
        $filter = $this->entityValues()->filter(function($item) use ($fieldMetaId) {
            if ($item->entity_fields_meta_id == $fieldMetaId)
                return true;
        });

        // If not value is found into the database, let's create a new empty one
        if ( ! $filter->count())
        {
            return new EntityFieldsValue([
                'module_type'           => $this->entityModule,
                'module_id'             => $this->id,
                'entity_fields_meta_id' => $fieldMetaId,
            ]);
        }

        return $filter->pop();
    }

    /**
     * Changes the value of an specified attribute.
     *
     * @param $name
     * @param $value
     */
    public function setEavAttribute($name, $value)
    {
        $this->eavAttributes[$name]['value'] = $value;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getEavAttribute($name)
    {
        return $this->eavAttributes[$name]->value;
    }

    /**
     * Returns the eavAttributes property
     *
     * @return array
     */
    public function getEavAttributes()
    {
        return $this->eavAttributes;
    }

    /**
     * @param $key
     * @return mixed
     */
    function __get($key)
    {
        if ($this->eavFieldExists($key))
        {
            return $this->getEavAttribute($key);
        }

        return parent::__get($key);
    }

    /**
     * Checking first if the variable to be set exists as an EAV field.
     *
     * @param $key
     * @param $value
     */
    function __set($key, $value)
    {
        if ($this->eavFieldExists($key))
        {
            return $this->setEavAttribute($key, $value);
        }

        return parent::__set($key, $value);
    }

    /**
     * Determine if an attribute exists on the EAV model.
     *
     * @param  string  $key
     * @return void
     */
    public function __isset($key)
    {
        return (isset($this->eavAttributes[$key]) || parent::__isset($key));
    }

    /**
     * Unset an attribute on the model.
     *
     * @param  string  $key
     * @return void
     */
    public function __unset($key)
    {
        unset($this->eavAttributes[$key]);

        parent::__unset($key);
    }

}