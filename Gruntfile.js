// Gruntfile
module.exports = function(grunt) {

    // Initializing the configuration object
    grunt.initConfig({

        copy: {
            dist: {
                files: [
                    { expand: true, flatten: true, src: ['./bower_components/font-awesome/fonts/*'],    dest: './public/assets/fonts/', filter: 'isFile' },
                    { expand: true, flatten: true, src: ['./bower_components/bootstrap/fonts/*'],       dest: './public/assets/fonts/', filter: 'isFile' }
                ]
            }
        },
        // Task configuration
        concat: {
            options: {
                //separator: ';'
            },
            dist: {
                src: [
                    // jQuery
                    './bower_components/jquery/dist/jquery.js',
                    './app/assets/javascript/jquery-ui-1.10.4.custom.min.js',
                    // Twitter Bootstrap
                    './bower_components/bootstrap/dist/js/bootstrap.js',
                    // AngularJS
                    './bower_components/angular/angular.js',
                    './bower_components/angular-resource/angular-resource.js',
                    // Froala editor
                    './bower_components/froala/js/froala_editor.min.js',
                    './bower_components/froala/js/langs/es.js',
                    // Clockpicker
                    './app/assets/javascript/clockpicker.js',
                    // Datepicker
                    './bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js',
                    './bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js',
                    // Misc
                    './app/assets/javascript/angular/angular.config.js',
                    './app/assets/javascript/custom.js',
                    './app/assets/javascript/angular/services/contactService.js',
                    './app/assets/javascript/angular/controllers/contact.controller.js',
                    './app/assets/javascript/angular/directives/select.entity.js',
                    './app/assets/javascript/angular/directives/activity.js'

                ],
                dest: './public/assets/javascript/scripts.min.js'
            }
        },
        uglify: {
            options: {
                mangle: false // For keeping the functions and variables names unchanged
            },
            dist: {
                files: {
                    './public/assets/javascript/scripts.min.js': './public/assets/javascript/scripts.min.js'
                }
            }
        },
        less: {
            dev: {
                options: {
                    paths: ["./app/assets/stylesheets"]
                    //compress: true,
                    //cleancss: true
                },
                files: {
                    "./public/assets/stylesheets/base.min.css": "./app/assets/stylesheets/base.less",
                    "./public/assets/stylesheets/main.min.css": "./app/assets/stylesheets/app.less"
                }
            }
        },
        watch: {
            less: {
                files: [
                    './app/assets/stylesheets/*.less',
                    './app/assets/stylesheets/structure/*.less',
                    './app/assets/stylesheets/components/*.less',
                    './app/assets/stylesheets/froala/*.less'
                ],
                tasks: ['less']
            },
            js: {
                files: [
                    //watched files
                    './bower_components/jquery/jquery.js',
                    './bower_components/bootstrap/dist/js/bootstrap.js',
                    './app/assets/javascript/*.js',
                    './app/assets/javascript/angular/*.js',
                    './app/assets/javascript/angular/controllers/*.js',
                    './app/assets/javascript/angular/services/*.js',
                    './app/assets/javascript/angular/directives/*.js'
                ],
                tasks: ['concat:dist']
                //tasks: ['concat:dist','uglify:dist']     //tasks to run
            },
        },
        clean: {
            dist: [
                './public/assets/stylesheets/main.min.css',
                './public/assets/javascript/scripts.min.js',
                './public/assets/fonts/*'
            ]
        }
    });

    // Plugin loading
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');

    // Register tasks
    grunt.registerTask('default', [
        'clean',
        'copy',
        'concat',
        'uglify',
        'less'
    ]);
    // Watch css
    grunt.registerTask('css', [
        'clean',
        'copy',
        'less'
    ]);
    grunt.registerTask('dev', [
        'watch'
    ]);

};